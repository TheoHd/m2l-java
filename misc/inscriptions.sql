-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Ven 12 jan. 2018 à 10:43
-- Version du serveur :  5.7.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `inscriptions_sportives_theo_aby`
--

DROP DATABASE IF EXISTS `inscriptions_sportives_theo_aby`;
CREATE DATABASE `inscriptions_sportives_theo_aby`;
USE `inscriptions_sportives_theo_aby`;

DROP TABLE IF EXISTS `personne`;
CREATE TABLE IF NOT EXISTS `personne` (
  `id_per` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `prenom` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `equipe`;
CREATE TABLE IF NOT EXISTS `equipe` (
  `id_e` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `competition`;
CREATE TABLE IF NOT EXISTS `competition` (
  `id_co` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nom` varchar(255) NOT NULL,
  `date_cloture` DATE NOT NULL,
  `en_equipe` boolean NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `candidat`;
CREATE TABLE IF NOT EXISTS `candidat` (
  `id_ca` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `participer`;
CREATE TABLE IF NOT EXISTS `participer` (
  `id_par` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_e` int(11),
  `id_ca` int(11),
  `id_co` int(11) NOT NULL,
  FOREIGN KEY (id_e) REFERENCES equipe(id_e),
  FOREIGN KEY (id_ca) REFERENCES candidat(id_ca),
  FOREIGN KEY (id_co) REFERENCES competition(id_co)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `appartenir`;
CREATE TABLE IF NOT EXISTS `appartenir` (
  `id_a` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_e` int(11) NOT NULL,
  `id_per` int(11) NOT NULL,
  FOREIGN KEY (id_e) REFERENCES equipe(id_e),
  FOREIGN KEY (id_per) REFERENCES personne(id_per) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `etre`;
CREATE TABLE IF NOT EXISTS `etre` (
  `id_etre` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_per` int(11) NOT NULL,
  `id_ca` int(11) NOT NULL,
  FOREIGN KEY (id_per) REFERENCES personne(id_per),
  FOREIGN KEY (id_ca) REFERENCES candidat(id_ca) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;