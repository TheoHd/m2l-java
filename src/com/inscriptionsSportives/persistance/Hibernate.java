package com.inscriptionsSportives.persistance;

import com.inscriptionsSportives.app.*;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe n'est utilisée que dans la couche persistance. Elle permet
 * d'echanger des donnees entre une base de donnee et les differentes classes.
 *
 * @author Théo
 */
public class Hibernate {

	private static final String HIBERNATE_CFG_PATH = "hibernate.cfg.xml";
	private static Hibernate hibernate;
	private static Session session;
	private static Transaction tx;
	private static Inscriptions inscriptions = Inscriptions.getInscriptions();

	private Hibernate() {
	}

	private static Session getSession() throws HibernateException {
		SessionFactory sessionFactory = null;
		try {
			Configuration configuration = new Configuration().configure(HIBERNATE_CFG_PATH);
			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
					.applySettings(configuration.getProperties()).build();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			return sessionFactory.openSession();
		} catch (RuntimeException e) {
			throw new RuntimeException();
		}
	}

	public static Hibernate getHibernate() {
		if (hibernate == null) {
			hibernate = new Hibernate();
		}
		return hibernate;
	}

	// public Object read(Object objet, int id) {
	// try {
	// session = sessionFactory.openSession();
	// tx = session.beginTransaction();
	// session.get(objet,id);
	// tx.commit();
	// } catch (Exception ex) {
	// ex.printStackTrace();
	// tx.rollback();
	// } finally {
	// session.close();
	// }
	// }

	public void updateEntitiesWithDatabase() {
		List<Equipe> equipes = getData("com.inscriptionsSportives.app.Equipe");
		List<Personne> personnes = getData("com.inscriptionsSportives.app.Personne");
		List<Competition> competitions = getData("com.inscriptionsSportives.app.Competition");
		List<Candidat> candidats = new ArrayList<>();
		candidats.addAll(personnes);
		candidats.addAll(equipes);
		if(!candidats.equals(null))
			inscriptions.setCandidats(candidats);
		if(!competitions.equals(null))
			inscriptions.setCompetitions(competitions);
	}

	/**
	 * Permet de sauvegarder un objet dans une base de donnée.
	 *
	 * @param objet
	 *            : Un objet de type Candidat, Equipe, Personne ou Competition
	 */
	public void save(Object objet) {
		try {
			session = getSession();
			tx = session.beginTransaction();
			session.save(objet);
			tx.commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			tx.rollback();
		} finally {
			session.close();
		}
	}

	/**
	 * Permet de supprimer un objet enregistrée en base de donnée
	 *
	 * @param objet
	 *            : Un objet de type Candidat, Equipe, Personne ou Competition
	 */
	public void delete(Object objet) {
		try {
			session = getSession();
			tx = session.beginTransaction();
			session.delete(objet);
			tx.commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			tx.rollback();
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getData(String className) {
		session = getSession();
		Query query = session.createQuery("from " + className);
		return new ArrayList<>((List<T>) query.list());
	}

	@SuppressWarnings("unchecked")
	public <T> T getData(String className, int id) {
		session = getSession();
		Query query = session.createQuery("from " + className + " where id = " + id);
		return (T) (query.list().get(0));
	}
}