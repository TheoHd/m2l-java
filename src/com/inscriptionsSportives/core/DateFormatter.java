package com.inscriptionsSportives.core;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateFormatter {
	
	public static Date localDateToDate(LocalDate localDate) {
		return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}
    public static LocalDate dateToLocalDate(Date date){
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
    public static String localDateToString(LocalDate localDate){
    	if(localDate != null) {
    		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-LLLL-yyyy");
	        String formattedString = localDate.format(formatter);
	        return formattedString;
    	}else {
    		return "";
    	}
    		
        
    }
}
