package com.inscriptionsSportives.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Functions {
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static String ucFirst(String word){
        return word.substring(0,1).toUpperCase() + word.substring(1);
    }

    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }
}
