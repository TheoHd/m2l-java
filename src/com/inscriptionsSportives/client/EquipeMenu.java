package com.inscriptionsSportives.client;

import com.inscriptionsSportives.app.*;
import commandLineMenus.*;
import commandLineMenus.rendering.examples.util.InOut;
import java.util.ArrayList;

public class EquipeMenu {

	/*
	 * PARTIE PRINCIPALE
	 */

	static Menu getEquipeMenu() {
		Menu equipeMenu = new Menu("- Menu Équipe -", "Gérer les équipes", "2");
		equipeMenu.add(getCreateEquipeOption());
		equipeMenu.add(getModifyEquipeOption());
		equipeMenu.add(getDeleteEquipeOption());
		equipeMenu.add(getReadMultipleEquipeOption());
		equipeMenu.addBack("0");
		return equipeMenu;
	}

	static Option getCreateEquipeOption() {
		return new Option("Créer une nouvelle équipe", "1", () -> {
			String nom = InOut.getString("Nom de l'équipe: ");
			Inscriptions.getInscriptions().createEquipe(nom);
		});
	}

	static Option getModifyEquipeOption() {
		List<Equipe> list = new List<>("Modifier une équipe", "2",
				() -> new ArrayList<>(Inscriptions.getInscriptions().getEquipes()),
				equipe -> getModifyEquipeOptionAction(equipe));

		list.addBack("0");
		return list;
	}

	static Option getDeleteEquipeOption() {
		List<Equipe> list = new List<>("Supprimer une équipe", "3",
				() -> new ArrayList<>(Inscriptions.getInscriptions().getEquipes()), (i, equipe) -> equipe.delete());
		list.addBack("0");
		return list;
	}

	static Option getReadMultipleEquipeOption() {
		return new Option("Voir toutes les équipes", "4",
				() -> System.out.println("-----\n" + Inscriptions.getInscriptions().getEquipes()));
	}

	/*
	 * PARTIE MODIFICATION
	 */

	static Option getModifyEquipeOptionAction(Equipe equipe) {
		Menu modifyEquipeMenu = new Menu(equipe.getNom() + " (" + equipe.getMembres().size() + ")");
		modifyEquipeMenu.add(getChangeEquipeNameOption(equipe));
		modifyEquipeMenu.add(getAddMembreToEquipeOption(equipe));
		modifyEquipeMenu.add(getRemoveMembreFromEquipeOption(equipe));
		modifyEquipeMenu.add(getReadMembreEquipeOption(equipe));
		modifyEquipeMenu.addBack("0");
		return modifyEquipeMenu;
	}

	static Option getChangeEquipeNameOption(Equipe equipe) {
		return new Option("Changer le nom de l'équipe", "1", () -> {
			equipe.setNom(InOut.getString("Nouveau nom de l'équipe: "));
		});
	}

	static Option getAddMembreToEquipeOption(Equipe equipe) {
		List<Personne> list = new List<>("Ajouter un membre à la compétition", "2", () -> {
			ArrayList<Personne> potentialNewMember = new ArrayList<>();
			for (Personne personne : Inscriptions.getInscriptions().getPersonnes())
				if (!equipe.getMembres().contains(personne))
					potentialNewMember.add(personne);
			return potentialNewMember;
		}, (i, personne) -> equipe.add(personne));
		list.addBack("0");
		return list;
	}

	static Option getRemoveMembreFromEquipeOption(Equipe equipe) {
		List<Personne> list = new List<>("Supprimer une personne de l'équipe", "3",
				() -> new ArrayList<>(equipe.getMembres()), (i, membre) -> equipe.remove(membre));
		list.addBack("0");
		return list;
	}

	static Option getReadMembreEquipeOption(Equipe equipe) {
		return new Option("Afficher les membres de l'équipe", "4",
				() -> System.out.println("-----\n" + equipe.toString() + "-----\n" + equipe.getMembres()));
	}

}
