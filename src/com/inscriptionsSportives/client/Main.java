package com.inscriptionsSportives.client;

import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.persistance.Hibernate;

public class Main {

	public static void main(String[] args) {
		if(Inscriptions.useHibernate()) {
			Hibernate.getHibernate().updateEntitiesWithDatabase();
		}
		MainMenu.getMainMenu().start();
	}

}
