package com.inscriptionsSportives.client;

import commandLineMenus.Action;
import commandLineMenus.Menu;
import commandLineMenus.Option;

class MainMenu {

	static Menu getMainMenu() {
		Menu mainMenu = new Menu("- Voulez-vous utiliser la version fenêtre ou console de l'application ? -");
		mainMenu.add(getConsoleMenu());
		mainMenu.add(getInterfaceGraphiqueOption());
		mainMenu.addQuit("0");
		return mainMenu;
	}

	static Menu getConsoleMenu() {
		Menu consoleMenu = new Menu("- Menu Console -", "Version console", "1");
		consoleMenu.add(CompetitionMenu.getCompetitionMenu());
		consoleMenu.add(EquipeMenu.getEquipeMenu());
		consoleMenu.add(PersonneMenu.getPersonneMenu());
		consoleMenu.addBack("0");
		return consoleMenu;
	}

	static Option getInterfaceGraphiqueOption() {
		return new Option("Version fenêtre", "2", getInterfaceGraphiqueAction());
	}

	static Action getInterfaceGraphiqueAction() {
		return new Action() {
			public void optionSelected() {
				new InterfaceGraphique();
			}
		};
	}

}
