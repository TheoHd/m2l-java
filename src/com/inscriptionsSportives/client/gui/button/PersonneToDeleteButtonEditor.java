package com.inscriptionsSportives.client.gui.button;

import javax.swing.*;


import com.inscriptionsSportives.app.Equipe;
import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.app.Personne;

import com.inscriptionsSportives.client.gui.table.PersonneTable;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PersonneToDeleteButtonEditor extends DefaultCellEditor {
	
	private static final long serialVersionUID = -5250344554723198851L;
	protected JButton button;
	private String label;
	private boolean isPushed;
	private Equipe equipe;
	private JTable modifiedTable;
	private int personneToRemoveNumber;

	public PersonneToDeleteButtonEditor(JCheckBox checkBox, Equipe equipe) {
		super(checkBox);
		this.equipe = equipe;
		button = new JButton();
		button.setOpaque(true);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fireEditingStopped();
			}
		});
	}

	private Equipe getEquipe() {
		return this.equipe;
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		if (isSelected) {
			button.setForeground(table.getSelectionForeground());
			button.setBackground(table.getSelectionBackground());
		} else {
			button.setForeground(table.getForeground());
			button.setBackground(table.getBackground());
		}
		label = (value == null) ? "" : value.toString();
		button.setText(label);
		isPushed = true;
		modifiedTable = table;
		return button;

	}

	@Override
	public Object getCellEditorValue() {
		if (isPushed) {
			personneToRemoveNumber = modifiedTable.getSelectedRow();
			int i = 0;
			for (Personne personne : Inscriptions.getInscriptions().getPersonnes()) {
				if (i == personneToRemoveNumber) {
					getEquipe().remove(personne);
					PersonneTable.getPersonneFromEquipeTableModel().removeRow(personneToRemoveNumber);
					PersonneTable.getPersonneToDeleteTableModel().removeRow(personneToRemoveNumber);
				}
				i++;
			}
		}
		isPushed = false;
		return label;
	}

	@Override
	public boolean stopCellEditing() {
		isPushed = false;
		return super.stopCellEditing();
	}
}