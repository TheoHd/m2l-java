package com.inscriptionsSportives.client.gui.button;

import javax.swing.*;

import com.inscriptionsSportives.app.Competition;
import com.inscriptionsSportives.app.Equipe;
import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.client.gui.table.EquipeTable;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EquipeToAddButtonEditor extends DefaultCellEditor {

	private static final long serialVersionUID = -6421205106596459328L;
	protected JButton button;
	private String label;
	private boolean isPushed;
	private Competition competition;
	private JTable modifiedTable;
	private int equipeToAddNumber;

	public EquipeToAddButtonEditor(JCheckBox checkBox, Competition competition) {
		super(checkBox);
		this.competition = competition;
		button = new JButton();
		button.setOpaque(true);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fireEditingStopped();
			}
		});
	}

	private Competition getCompetition() {
		return this.competition;
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		if (isSelected) {
			button.setForeground(table.getSelectionForeground());
			button.setBackground(table.getSelectionBackground());
		} else {
			button.setForeground(table.getForeground());
			button.setBackground(table.getBackground());
		}
		label = (value == null) ? "" : value.toString();
		button.setText(label);
		isPushed = true;
		modifiedTable = table;
		return button;

	}

	@Override
	public Object getCellEditorValue() {
		if (isPushed) {
			equipeToAddNumber = modifiedTable.getSelectedRow();
			int i = 0;
			for (Equipe equipe : Inscriptions.getInscriptions().getEquipes()) {
				if (i == equipeToAddNumber) {
					getCompetition().add(equipe);
					Object[] row = new Object[2];
					row[0] = EquipeTable.getEquipeFromCompetitionTableModel().getRowCount() + 1;
					row[1] = equipe.getNom();
					EquipeTable.getEquipeFromCompetitionTableModel().addRow(row);
				}
				i++;
			}
		}
		isPushed = false;
		return label;
	}

	@Override
	public boolean stopCellEditing() {
		isPushed = false;
		return super.stopCellEditing();
	}
}