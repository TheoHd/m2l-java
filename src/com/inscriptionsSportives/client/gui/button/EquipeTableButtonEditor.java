package com.inscriptionsSportives.client.gui.button;

import com.inscriptionsSportives.client.gui.view.EquipeGuiMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EquipeTableButtonEditor extends DefaultCellEditor {

	private static final long serialVersionUID = -2728286795194981150L;
	protected JButton button;
	protected JTable modifiedTable;
	private String label;
	private boolean isPushed;

	public EquipeTableButtonEditor(JCheckBox checkBox) {
		super(checkBox);
		button = new JButton();
		button.setOpaque(true);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fireEditingStopped();
			}
		});
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

		if (isSelected) {
			button.setForeground(table.getSelectionForeground());
			button.setBackground(table.getSelectionBackground());
		} else {
			button.setForeground(table.getForeground());
			button.setBackground(table.getBackground());
		}
		label = (value == null) ? "" : value.toString();
		button.setText(label);
		isPushed = true;
		modifiedTable = table;
		return button;
	}

	@Override
	public Object getCellEditorValue() {
		if (isPushed) {
			int modifiedEquipeNumber = modifiedTable.getSelectedRow();
			EquipeGuiMenu.getEquipeMenu().newEquipeGuiMenu(modifiedEquipeNumber);
		}
		isPushed = false;
		return label;
	}

	@Override
	public boolean stopCellEditing() {
		isPushed = false;
		return super.stopCellEditing();
	}
}