package com.inscriptionsSportives.client.gui.button;

import javax.swing.*;

import com.inscriptionsSportives.app.Competition;
import com.inscriptionsSportives.app.Equipe;
import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.client.gui.table.EquipeTable;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EquipeToDeleteButtonEditor extends DefaultCellEditor {
	
	private static final long serialVersionUID = -5250344554723198851L;
	protected JButton button;
	private String label;
	private boolean isPushed;
	private Competition competition;
	private JTable modifiedTable;
	private int equipeToRemoveNumber;

	public EquipeToDeleteButtonEditor(JCheckBox checkBox, Competition competition) {
		super(checkBox);
		this.competition = competition;
		button = new JButton();
		button.setOpaque(true);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fireEditingStopped();
			}
		});
	}

	private Competition getCompetition() {
		return this.competition;
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		if (isSelected) {
			button.setForeground(table.getSelectionForeground());
			button.setBackground(table.getSelectionBackground());
		} else {
			button.setForeground(table.getForeground());
			button.setBackground(table.getBackground());
		}
		label = (value == null) ? "" : value.toString();
		button.setText(label);
		isPushed = true;
		modifiedTable = table;
		return button;

	}

	@Override
	public Object getCellEditorValue() {
		if (isPushed) {
			equipeToRemoveNumber = modifiedTable.getSelectedRow();
			int i = 0;
			for (Equipe equipe : Inscriptions.getInscriptions().getEquipes()) {
				if (i == equipeToRemoveNumber) {
					getCompetition().remove(equipe);
					EquipeTable.getEquipeFromCompetitionTableModel().removeRow(equipeToRemoveNumber);
					EquipeTable.getEquipeToDeleteTableModel().removeRow(equipeToRemoveNumber);
				}
				i++;
			}
		}
		isPushed = false;
		return label;
	}

	@Override
	public boolean stopCellEditing() {
		isPushed = false;
		return super.stopCellEditing();
	}
}