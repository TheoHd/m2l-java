package com.inscriptionsSportives.client.gui.tabbedPane;

import com.inscriptionsSportives.client.gui.table.CompetitionTable;
import com.inscriptionsSportives.client.gui.table.EquipeTable;
import com.inscriptionsSportives.client.gui.table.PersonneTable;

import javax.swing.*;

public class InscriptionsTabbedPane extends JTabbedPane {

	private static final long serialVersionUID = 3260058823475016027L;

	public InscriptionsTabbedPane(CompetitionTable competitionTable, EquipeTable equipeTable,
			PersonneTable personneTable) {

		addTab("Compétition", null, new JScrollPane(competitionTable), "Permet de gérer les compétitions");
		addTab("Équipe", null, new JScrollPane(equipeTable), "Permet de gérer les équipes");
		addTab("Personne", null, new JScrollPane(personneTable), "Permet de gérer les personnes");
	}

}
