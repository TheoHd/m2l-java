package com.inscriptionsSportives.client.gui.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;

import com.inscriptionsSportives.app.Competition;
import com.inscriptionsSportives.client.gui.table.EquipeTable;

public class EquipeGuiList {

	private static EquipeGuiList equipeGuiList;
	
	private JPanel equipeListPanel;
	private JTable equipeListTable;
	
	private JPanel equipeDeleteListPanel;
	private JTable equipeDeleteListTable;

	public void newEquipeGuiList(Competition competition) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		JFrame frame = new JFrame("Liste des équipes");
		createView(frame,competition);
		frame.setContentPane(equipeListPanel);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setTitle("Menu Compétition");
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setResizable(false);
	}
	
	public void newDeleteEquipeGuiList(Competition competition) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		JFrame frame = new JFrame("Liste des équipes à supprimer");
		createViewDelete(frame,competition);
		frame.setContentPane(equipeDeleteListPanel);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setTitle("Menu Compétition");
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setResizable(false);
	}
	
	private void createViewDelete(JFrame frame, Competition competition) {
		equipeDeleteListPanel = new JPanel();
		equipeDeleteListTable = EquipeTable.getEquipeToDeleteTable(competition);		
		equipeDeleteListPanel.add(new JScrollPane(equipeDeleteListTable));			
	}
	
	private void createView(JFrame frame, Competition competition) {
		equipeListPanel = new JPanel();
		equipeListTable = EquipeTable.getEquipeToAddTable(competition);		
		equipeListPanel.add(new JScrollPane(equipeListTable));		
	}
	
	public static EquipeGuiList getEquipeList() {
		if (equipeGuiList == null)
			equipeGuiList = new EquipeGuiList();
		return equipeGuiList;
	}
}
