package com.inscriptionsSportives.client.gui.view;


import com.inscriptionsSportives.app.Equipe;
import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.client.gui.table.EquipeTable;

import javax.swing.*;
import java.awt.*;

public class EquipeGuiMenu {

	private static EquipeGuiMenu equipeGuiMenu;

	private static int equipeNumber;
	private Equipe equipe;

	private String nomEquipe;

	private JTextField nomTextField;
	
	private JPanel equipeMenuPanel;
	private JPanel equipeFormPanel;
	private JPanel equipeActionPanel;

	private JLabel nomLabel;

	private JButton supprimerButton;
	private JButton editerButton;
	private JButton annulerButton;

	Equipe getEquipe() {
		return this.equipe;
	}

	public void newEquipeGuiMenu(int eqNumber) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		JFrame frame = new JFrame("Equipe Menu");
		equipeNumber = eqNumber;
		int i = 0;
		for (Equipe equipe: Inscriptions.getInscriptions().getEquipes()) {
			if (equipeNumber == i) {
				this.equipe= equipe;
				this.nomEquipe= equipe.getNom();
			}
			i++;
			equipe.toString();
		}
		createView(frame);
		frame.setContentPane(equipeMenuPanel);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setTitle("Menu Equipe");
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setResizable(false);
	}

	private void createView(JFrame frame) {
		equipeMenuPanel = new JPanel();
		equipeFormPanel = new JPanel();
		equipeActionPanel = new JPanel();

		nomTextField = new JTextField(nomEquipe);

		
		nomLabel = new JLabel("Nom de l'équipe");
		
		
		supprimerButton = new JButton("Supprimer");
		editerButton = new JButton("Éditer");
		annulerButton = new JButton("Annuler");

		supprimerButton.addActionListener((e) -> {
			int dialogSupprimerConfirmation = JOptionPane.YES_NO_OPTION;
			JOptionPane.showConfirmDialog(null, "Êtes-vous sûr de vouloir supprimer cette équipe ?", "Warning",
					dialogSupprimerConfirmation);
			if (dialogSupprimerConfirmation == JOptionPane.YES_OPTION) {
				Inscriptions.getInscriptions().remove(equipe);
				EquipeTable.getEquipeTableModel().removeRow(equipeNumber);
				frame.dispose();
			} else if (dialogSupprimerConfirmation == JOptionPane.YES_OPTION) {
				JOptionPane.getRootFrame().dispose();
			}
		});
		editerButton.addActionListener((e) -> {
			int dialogModifierConfirmation = JOptionPane.YES_NO_OPTION;
			JOptionPane.showConfirmDialog(null, "Êtes-vous sûr de vouloir modifier cette équipe ?", "Warning",
					JOptionPane.YES_NO_OPTION);
			if (dialogModifierConfirmation == JOptionPane.YES_OPTION) {
				equipe.setNom(nomTextField.getText());
				EquipeTable.getEquipeTableModel().setValueAt(""+(equipeNumber+1),equipeNumber,0);
				EquipeTable.getEquipeTableModel().setValueAt(nomTextField.getText(),equipeNumber,1);
				Inscriptions.getInscriptions().sauvegarder();
				JOptionPane.showMessageDialog(null, "L'équipe a bien été modifiée", "Information", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		annulerButton.addActionListener((e) -> {
			frame.dispose();
		});

		equipeMenuPanel.setLayout(new BoxLayout(equipeMenuPanel, BoxLayout.Y_AXIS));
		equipeMenuPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		equipeFormPanel.setLayout(new GridLayout(8, 2));
		equipeActionPanel.setLayout(new FlowLayout());

		equipeFormPanel.add(nomLabel);
		equipeFormPanel.add(nomTextField);

		equipeActionPanel.add(editerButton);
		equipeActionPanel.add(supprimerButton);
		equipeActionPanel.add(annulerButton);

		equipeMenuPanel.add(equipeFormPanel);
		equipeMenuPanel.add(equipeActionPanel);
	}

	public static EquipeGuiMenu getEquipeMenu() {
		if (equipeGuiMenu == null) {
			equipeGuiMenu = new EquipeGuiMenu();
		}
		return equipeGuiMenu;
	}

	public static int getEquipeNumber() {
		return equipeNumber;
	}

}
