package com.inscriptionsSportives.client.gui.view;


import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.app.Personne;

import com.inscriptionsSportives.client.gui.table.PersonneTable;

import javax.swing.*;
import java.awt.*;

public class PersonneGuiMenu {

	private static PersonneGuiMenu personneGuiMenu;

	private static int personneNumber;
	private Personne personne;

	private String nomPersonne;
	private String prenomPersonne;
	private String emailPersonne;

	private JTextField nomTextField;
	private JTextField prenomTextField;
	private JTextField emailTextField;
	
	private JPanel personneMenuPanel;
	private JPanel personneFormPanel;
	private JPanel personneActionPanel;

	private JLabel nomLabel;
	private JLabel prenomLabel;
	private JLabel emailLabel;

	private JButton supprimerButton;
	private JButton editerButton;
	private JButton annulerButton;

	Personne getPersonne() {
		return this.personne;
	}

	public void newPersonneGuiMenu(int persNumber) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		JFrame frame = new JFrame("Personne Menu");
		personneNumber = persNumber;
		int i = 0;
		for (Personne personne : Inscriptions.getInscriptions().getPersonnes()) {
			if (personneNumber == i) {
				this.personne = personne;
				this.nomPersonne = personne.getNom();
				this.prenomPersonne = personne.getPrenom();
				this.emailPersonne= personne.getEmail();
			}
			i++;
			personne.toString();
		}
		createView(frame);
		frame.setContentPane(personneMenuPanel);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setTitle("Menu Personne");
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setResizable(false);
	}

	private void createView(JFrame frame) {
		personneMenuPanel = new JPanel();
		personneFormPanel = new JPanel();
		personneActionPanel = new JPanel();

		nomTextField = new JTextField(nomPersonne);
		prenomTextField = new JTextField(prenomPersonne);
		emailTextField = new JTextField(emailPersonne);

		
		nomLabel = new JLabel("Nom de la personne");
		prenomLabel = new JLabel("Prénom de la personne");
		emailLabel = new JLabel("E-mail de la personne");
		
		
		supprimerButton = new JButton("Supprimer");
		editerButton = new JButton("Éditer");
		annulerButton = new JButton("Annuler");

		supprimerButton.addActionListener((e) -> {
			int dialogSupprimerConfirmation = JOptionPane.YES_NO_OPTION;
			JOptionPane.showConfirmDialog(null, "Êtes-vous sûr de vouloir supprimer cette personne ?", "Warning",
					dialogSupprimerConfirmation);
			if (dialogSupprimerConfirmation == JOptionPane.YES_OPTION) {
				Inscriptions.getInscriptions().remove(personne);
				PersonneTable.getPersonneTableModel().removeRow(personneNumber);
				frame.dispose();
			} else if (dialogSupprimerConfirmation == JOptionPane.YES_OPTION) {
				JOptionPane.getRootFrame().dispose();
			}
		});
		editerButton.addActionListener((e) -> {
			int dialogModifierConfirmation = JOptionPane.YES_NO_OPTION;
			JOptionPane.showConfirmDialog(null, "Êtes-vous sûr de vouloir modifier cette personne ?", "Warning",
					JOptionPane.YES_NO_OPTION);
			if (dialogModifierConfirmation == JOptionPane.YES_OPTION) {
				personne.setNom(nomTextField.getText());
				personne.setPrenom(prenomTextField.getText());
				personne.setEmail(emailTextField.getText());
				PersonneTable.getPersonneTableModel().setValueAt(""+(personneNumber+1),personneNumber,0);
				PersonneTable.getPersonneTableModel().setValueAt(nomTextField.getText(),personneNumber,1);
				PersonneTable.getPersonneTableModel().setValueAt(prenomTextField.getText(),personneNumber,2);
				PersonneTable.getPersonneTableModel().setValueAt(emailTextField.getText(),personneNumber,3);
				Inscriptions.getInscriptions().sauvegarder();
				JOptionPane.showMessageDialog(null, "La personne a bien été modifiée", "Information", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		annulerButton.addActionListener((e) -> {
			frame.dispose();
		});

		personneMenuPanel.setLayout(new BoxLayout(personneMenuPanel, BoxLayout.Y_AXIS));
		personneMenuPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		personneFormPanel.setLayout(new GridLayout(8, 2));
		personneActionPanel.setLayout(new FlowLayout());

		personneFormPanel.add(nomLabel);
		personneFormPanel.add(nomTextField);
		personneFormPanel.add(prenomLabel);
		personneFormPanel.add(prenomTextField);
		personneFormPanel.add(emailLabel);
		personneFormPanel.add(emailTextField);

		personneActionPanel.add(editerButton);
		personneActionPanel.add(supprimerButton);
		personneActionPanel.add(annulerButton);

		personneMenuPanel.add(personneFormPanel);
		personneMenuPanel.add(personneActionPanel);
	}

	public static PersonneGuiMenu getPersonneMenu() {
		if (personneGuiMenu == null) {
			personneGuiMenu = new PersonneGuiMenu();
		}
		return personneGuiMenu;
	}

	public static int getPersonneNumber() {
		return personneNumber;
	}

}
