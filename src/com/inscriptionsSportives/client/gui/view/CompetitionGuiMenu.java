package com.inscriptionsSportives.client.gui.view;

import com.inscriptionsSportives.app.Competition;
import com.inscriptionsSportives.app.Inscriptions;

import com.inscriptionsSportives.client.gui.table.CompetitionTable;
import com.inscriptionsSportives.client.gui.table.EquipeTable;
import com.inscriptionsSportives.core.DateFormatter;
import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import java.awt.*;
import java.util.Date;

public class CompetitionGuiMenu {

	private static CompetitionGuiMenu competitionGuiMenu;

	private static int competitionNumber;
	private Competition competition;

	private String nomCompetition;
	private Date dateDebut;
	private Date dateFin;
	private boolean isEnEquipe;

	private JTextField nomTextField;
	private JDateChooser dateDebutDateChooser;
	private JDateChooser dateFinDateChooser;

	private ButtonGroup choixEnEquipe;
	private JRadioButton enSoloRadioButton;
	private JRadioButton enEquipeRadioButton;

	private JPanel competitionMenuPanel;
	private JPanel competitionFormPanel;
	private JPanel competitionActionPanel;
	private JPanel competitionTablePanel;

	private JLabel nomLabel;
	private JLabel dateDebutLabel;
	private JLabel dateFinLabel;

	private JButton supprimerButton;
	private JButton editerButton;
	private JButton annulerButton;
	private JButton ajouterUneEquipeButton;
	private JButton supprimerUneEquipeButton;
	private JTable competitionEquipeTable;

	Competition getCompetition() {
		return this.competition;
	}

	public void newCompetitionGuiMenu(int competNumber) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		JFrame frame = new JFrame("CompetitionMenu");
		competitionNumber = competNumber;
		int i = 0;
		for (Competition competition : Inscriptions.getInscriptions().getCompetitions()) {
			if (competitionNumber == i) {
				this.competition = competition;
				this.nomCompetition = competition.getNom();
				if (competition.getDateDebut() != null)
					this.dateDebut = DateFormatter.localDateToDate(competition.getDateDebut());
				if (competition.getDateFin() != null)
					this.dateFin = DateFormatter.localDateToDate(competition.getDateFin());
				this.isEnEquipe = competition.isEnEquipe();
			}
			i++;
			competition.toString();
		}
		createView(frame);
		frame.setContentPane(competitionMenuPanel);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setTitle("Menu Compétition");
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setResizable(false);
	}

	private void createView(JFrame frame) {
		competitionMenuPanel = new JPanel();
		competitionFormPanel = new JPanel();
		competitionActionPanel = new JPanel();
		competitionTablePanel = new JPanel();

		nomTextField = new JTextField(nomCompetition);
		dateDebutDateChooser = new JDateChooser();
		dateDebutDateChooser.setDate(dateDebut);
		dateFinDateChooser = new JDateChooser();
		dateFinDateChooser.setDate(dateFin);

		nomLabel = new JLabel("Nom de la compétition");
		dateDebutLabel = new JLabel("Date de début de la compétition");
		dateFinLabel = new JLabel("Date de fin de la compétition");

		choixEnEquipe = new ButtonGroup();
		enSoloRadioButton = new JRadioButton("En solo");
		enEquipeRadioButton = new JRadioButton("En équipe");
		choixEnEquipe.add(enSoloRadioButton);
		choixEnEquipe.add(enEquipeRadioButton);

		if (this.isEnEquipe) {
			enEquipeRadioButton.setSelected(true);
			enSoloRadioButton.setSelected(false);
		} else {
			enEquipeRadioButton.setSelected(false);
			enSoloRadioButton.setSelected(true);
		}
		supprimerButton = new JButton("Supprimer");
		editerButton = new JButton("Éditer");
		annulerButton = new JButton("Annuler");
		ajouterUneEquipeButton = new JButton("Ajouter une équipe à la compétition");
		supprimerUneEquipeButton = new JButton("Supprimer une équipe de la compétition");

		supprimerButton.addActionListener((e) -> {
			int dialogSupprimerConfirmation = JOptionPane.YES_NO_OPTION;
			JOptionPane.showConfirmDialog(null, "Êtes-vous sûr de vouloir supprimer cette compétition ?", "Warning",
					dialogSupprimerConfirmation);
			if (dialogSupprimerConfirmation == JOptionPane.YES_OPTION) {
				Inscriptions.getInscriptions().remove(competition);
				CompetitionTable.getCompetitionTableModel().removeRow(competitionNumber);
				frame.dispose();
			} else if (dialogSupprimerConfirmation == JOptionPane.YES_OPTION) {
				JOptionPane.getRootFrame().dispose();
			}
		});
		editerButton.addActionListener((e) -> {
			int dialogModifierConfirmation = JOptionPane.YES_NO_OPTION;
			JOptionPane.showConfirmDialog(null, "Êtes-vous sûr de vouloir modifier cette compétition ?", "Warning",
					JOptionPane.YES_NO_OPTION);
			if (dialogModifierConfirmation == JOptionPane.YES_OPTION) {
				competition.setNom(nomTextField.getText());
				competition.setDateDebut(DateFormatter.dateToLocalDate(dateDebutDateChooser.getDate()));
				competition.setDateFin(DateFormatter.dateToLocalDate(dateFinDateChooser.getDate()));
				CompetitionTable.getCompetitionTableModel().setValueAt("" + (competitionNumber + 1), competitionNumber,
						0);
				CompetitionTable.getCompetitionTableModel().setValueAt(nomTextField.getText(), competitionNumber, 1);
				CompetitionTable.getCompetitionTableModel().setValueAt(
						DateFormatter.localDateToString(DateFormatter.dateToLocalDate(dateDebutDateChooser.getDate())),
						competitionNumber, 2);
				CompetitionTable.getCompetitionTableModel().setValueAt(
						DateFormatter.localDateToString(DateFormatter.dateToLocalDate(dateFinDateChooser.getDate())),
						competitionNumber, 3);
				if (enEquipeRadioButton.isSelected()) {
					competition.setEnEquipe(true);
					CompetitionTable.getCompetitionTableModel().setValueAt("En Équipe", competitionNumber, 4);
				} else {
					competition.setEnEquipe(false);
					CompetitionTable.getCompetitionTableModel().setValueAt("En Solo", competitionNumber, 4);
				}
				Inscriptions.getInscriptions().sauvegarder();
				JOptionPane.showMessageDialog(null, "La compétition a bien été modifiée", "Information",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		annulerButton.addActionListener((e) -> {
			frame.dispose();
		});
		ajouterUneEquipeButton.addActionListener((e) -> {
			EquipeGuiList.getEquipeList().newEquipeGuiList(getCompetition());
		});
		supprimerUneEquipeButton.addActionListener((e) -> {
			EquipeGuiList.getEquipeList().newDeleteEquipeGuiList(getCompetition());
		});

		competitionEquipeTable = EquipeTable.getEquipeFromCompetition(competition);
		competitionEquipeTable.setPreferredSize(new Dimension(300, 300));

		competitionMenuPanel.setLayout(new BoxLayout(competitionMenuPanel, BoxLayout.Y_AXIS));
		competitionMenuPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		competitionFormPanel.setLayout(new GridLayout(8, 2));
		competitionActionPanel.setLayout(new FlowLayout());
		competitionTablePanel.setLayout(new GridLayout(1, 1));

		competitionFormPanel.add(nomLabel);
		competitionFormPanel.add(nomTextField);
		competitionFormPanel.add(dateDebutLabel);
		competitionFormPanel.add(dateDebutDateChooser);
		competitionFormPanel.add(dateFinLabel);
		competitionFormPanel.add(dateFinDateChooser);
		competitionFormPanel.add(enSoloRadioButton);
		competitionFormPanel.add(enEquipeRadioButton);

		competitionActionPanel.add(editerButton);
		competitionActionPanel.add(supprimerButton);
		competitionActionPanel.add(annulerButton);

		competitionActionPanel.add(ajouterUneEquipeButton);
		competitionActionPanel.add(supprimerUneEquipeButton);

		competitionTablePanel.add(new JScrollPane(competitionEquipeTable));

		competitionMenuPanel.add(competitionFormPanel);
		competitionMenuPanel.add(competitionActionPanel);
		competitionMenuPanel.add(competitionTablePanel);
	}

	public static CompetitionGuiMenu getCompetitionMenu() {
		if (competitionGuiMenu == null) {
			competitionGuiMenu = new CompetitionGuiMenu();
		}
		return competitionGuiMenu;
	}

	public static int getCompetitionNumber() {
		return competitionNumber;
	}

}
