package com.inscriptionsSportives.client.gui.table;

import com.inscriptionsSportives.app.Competition;
import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.client.gui.button.ButtonRenderer;
import com.inscriptionsSportives.client.gui.button.CompetitionTableButtonEditor;
import com.inscriptionsSportives.core.DateFormatter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class CompetitionTable extends JTable {

	private static final long serialVersionUID = -1571495781953738946L;
	private static DefaultTableModel competitionTableModel;

	public CompetitionTable() {
		competitionTableModel = new DefaultTableModel(
				new String[] { "ID", "Nom de la compétition", "Date de début", "Date de fin", "Type", "Menu d'action" },
				0);
		updateCompetitionTableModel(competitionTableModel);
		setModel(competitionTableModel);
		getColumn("Menu d'action").setCellRenderer(new ButtonRenderer());
		getColumn("Menu d'action").setCellEditor(new CompetitionTableButtonEditor(new JCheckBox()));
	}

	private void updateCompetitionTableModel(DefaultTableModel competitionTableModel) {
		String[] row = new String[5];
		int i = 1;
		for (Competition competition : Inscriptions.getInscriptions().getCompetitions()) {
			row[0] = "" + i;
			row[1] = competition.getNom();
			row[2] = DateFormatter.localDateToString(competition.getDateDebut());
			row[3] = DateFormatter.localDateToString(competition.getDateFin());
			if (competition.isEnEquipe())
				row[4] = "En Équipe";
			else
				row[4] = "En Solo";
			competitionTableModel.addRow(row);
			i++;
		}
	}

	public static DefaultTableModel getCompetitionTableModel() {
		return competitionTableModel;
	}
	
	public void update() {
		repaint();
	}
}
