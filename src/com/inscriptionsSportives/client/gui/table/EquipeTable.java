package com.inscriptionsSportives.client.gui.table;

import com.inscriptionsSportives.app.Competition;
import com.inscriptionsSportives.app.Equipe;
import com.inscriptionsSportives.app.Inscriptions;

import com.inscriptionsSportives.client.gui.button.ButtonRenderer;
import com.inscriptionsSportives.client.gui.button.EquipeTableButtonEditor;
import com.inscriptionsSportives.client.gui.button.EquipeToAddButtonEditor;
import com.inscriptionsSportives.client.gui.button.EquipeToDeleteButtonEditor;

import java.awt.Dimension;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class EquipeTable extends JTable {

	private static final long serialVersionUID = -4830550058823237532L;
	private static DefaultTableModel equipeTableModel;
	private static DefaultTableModel equipeFromCompetitionTableModel;
	private static DefaultTableModel equipeToDeleteTableModel;

	public EquipeTable() {
		equipeTableModel = new DefaultTableModel(new String[] { "ID", "Nom de l'équipe", "Menu d'action" }, 0);
		updateEquipeTableModel(equipeTableModel);
		setModel(equipeTableModel);
		getColumn("Menu d'action").setCellRenderer(new ButtonRenderer());
		getColumn("Menu d'action").setCellEditor(new EquipeTableButtonEditor(new JCheckBox()));
	}

	private void updateEquipeTableModel(DefaultTableModel equipeTableModel) {
		String[] row = new String[3];
		int i = 1;
		for (Equipe equipe : Inscriptions.getInscriptions().getEquipes()) {
			row[0] = "" + i;
			row[1] = equipe.getNom();
			equipeTableModel.addRow(row);
			i++;
		}
	}

	public static DefaultTableModel getEquipeFromCompetitionTableModel() {
		return equipeFromCompetitionTableModel;
	}

	public static DefaultTableModel getEquipeToDeleteTableModel() {
		return equipeToDeleteTableModel;
	}

	public static DefaultTableModel getEquipeTableModel() {
		return equipeTableModel;
	}

	private static void updateEquipeToAddTableModel(Competition competition, DefaultTableModel equipeToAddTableModel) {
		String[] row = new String[3];
		int i = 1;
		for (Equipe equipe : Inscriptions.getInscriptions().getEquipes()) {
			if (!(equipe.getCompetitions().contains(competition))) {
				row[0] = "" + i;
				row[1] = equipe.getNom();
				equipeToAddTableModel.addRow(row);
				i++;
			}
		}
	}

	private static void updateEquipeFromCompetitionTableModel(Competition competition,
			DefaultTableModel equipeFromCompetitionTableModel) {
		String[] row = new String[3];
		int i = 1;
		for (Equipe equipe : Inscriptions.getInscriptions().getEquipes()) {
			if (equipe.getCompetitions().contains(competition)) {
				row[0] = "" + i;
				row[1] = equipe.getNom();
				equipeFromCompetitionTableModel.addRow(row);
				i++;
			}
		}
	}

	private static void updateEquipeToRemoveFromCompetitionTableModel(Competition competition,
			DefaultTableModel equipeFromCompetitionTableModel) {
		String[] row = new String[3];
		int i = 1;
		for (Equipe equipe : Inscriptions.getInscriptions().getEquipes()) {
			if (equipe.getCompetitions().contains(competition)) {
				row[0] = "" + i;
				row[1] = equipe.getNom();
				equipeFromCompetitionTableModel.addRow(row);
				i++;
			}
		}
	}

	public static JTable getEquipeToAddTable(Competition competition) {
		JTable equipeToAdd = new JTable();
		DefaultTableModel equipeTableModel = new DefaultTableModel(
				new String[] { "ID", "Nom de l'équipe", "Menu d'action" }, 0);
		updateEquipeToAddTableModel(competition, equipeTableModel);
		equipeToAdd.setModel(equipeTableModel);
		equipeToAdd.getColumn("Menu d'action").setCellRenderer(new ButtonRenderer());
		equipeToAdd.getColumn("Menu d'action").setCellEditor(new EquipeToAddButtonEditor(new JCheckBox(), competition));
		equipeToAdd.setPreferredSize(new Dimension(300, 300));
		return equipeToAdd;
	}

	public static JTable getEquipeToDeleteTable(Competition competition) {
		JTable equipeToRemove = new JTable();
		equipeToDeleteTableModel = new DefaultTableModel(new String[] { "ID", "Nom de l'équipe", "Menu d'action" }, 0);
		updateEquipeToRemoveFromCompetitionTableModel(competition, equipeToDeleteTableModel);
		equipeToRemove.setModel(equipeToDeleteTableModel);
		equipeToRemove.getColumn("Menu d'action").setCellRenderer(new ButtonRenderer());
		equipeToRemove.getColumn("Menu d'action")
				.setCellEditor(new EquipeToDeleteButtonEditor(new JCheckBox(), competition));
		equipeToRemove.setPreferredSize(new Dimension(300, 300));
		return equipeToRemove;
	}

	public static JTable getEquipeFromCompetition(Competition competition) {
		JTable equipeFromCompetition = new JTable();
		equipeFromCompetitionTableModel = new DefaultTableModel(new String[] { "ID", "Nom de l'équipe" }, 0);
		updateEquipeFromCompetitionTableModel(competition, equipeFromCompetitionTableModel);
		equipeFromCompetition.setModel(equipeFromCompetitionTableModel);
		equipeFromCompetition.setPreferredSize(new Dimension(300, 300));
		return equipeFromCompetition;
	}

}
