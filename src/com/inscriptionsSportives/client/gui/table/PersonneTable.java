package com.inscriptionsSportives.client.gui.table;

import com.inscriptionsSportives.app.Equipe;
import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.app.Personne;
import com.inscriptionsSportives.client.gui.button.ButtonRenderer;
import com.inscriptionsSportives.client.gui.button.PersonneToAddButtonEditor;
import com.inscriptionsSportives.client.gui.button.PersonneToDeleteButtonEditor;
import com.inscriptionsSportives.client.gui.button.PersonneTableButtonEditor;

import java.awt.Dimension;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class PersonneTable extends JTable {

	private static final long serialVersionUID = -1541706992090005633L;
	private static DefaultTableModel personneTableModel;
	private static DefaultTableModel personneFromEquipeTableModel;
	private static DefaultTableModel personneToDeleteTableModel;

	public PersonneTable() {
		personneTableModel = new DefaultTableModel(new String[] { "ID", "Nom de la personne", "Prénom de la personne",
				"E-mail de la personne", "Menu d'action" }, 0);
		updateCompetitionTableModel(personneTableModel);
		setModel(personneTableModel);
		getColumn("Menu d'action").setCellRenderer(new ButtonRenderer());
		getColumn("Menu d'action").setCellEditor(new PersonneTableButtonEditor(new JCheckBox()));
	}

	private void updateCompetitionTableModel(DefaultTableModel competitionTableModel) {
		String[] row = new String[4];
		int i = 1;
		for (Personne personne : Inscriptions.getInscriptions().getPersonnes()) {
			row[0] = "" + i;
			row[1] = personne.getNom();
			row[2] = personne.getPrenom();
			row[3] = personne.getEmail();
			competitionTableModel.addRow(row);
			i++;
		}
	}

	public static DefaultTableModel getPersonneTableModel() {
		return personneTableModel;
	}

	public static DefaultTableModel getPersonneFromEquipeTableModel() {
		return personneFromEquipeTableModel;
	}

	public static DefaultTableModel getPersonneToDeleteTableModel() {
		return personneToDeleteTableModel;
	}

	private static void updatePersonneToAddTableModel(Equipe equipe, DefaultTableModel personneToAddTableModel) {
		String[] row = new String[3];
		int i = 1;
		for (Personne personne : Inscriptions.getInscriptions().getPersonnes()) {
			if (!(personne.getEquipes().contains(equipe))) {
				row[0] = "" + i;
				row[1] = personne.getNom();
				row[2] = personne.getPrenom();
				row[3] = personne.getEmail();
				personneToAddTableModel.addRow(row);
				i++;
			}
		}
	}

	private static void updatePersonneFromEquipeTableModel(Equipe equipe, DefaultTableModel personneFromEquipe) {
		String[] row = new String[5];
		int i = 1;
		for (Personne personne : Inscriptions.getInscriptions().getPersonnes()) {
			if (personne.getEquipes().contains(equipe)) {
				row[0] = "" + i;
				row[1] = personne.getNom();
				row[2] = personne.getPrenom();
				row[3] = personne.getEmail();
				personneFromEquipe.addRow(row);
				i++;
			}
		}
	}

	private static void updatePersonneToRemoveFromEquipeTableModel(Equipe equipe,
			DefaultTableModel personneFromEquipeTableModel) {
		String[] row = new String[5];
		int i = 1;
		for (Personne personne : Inscriptions.getInscriptions().getPersonnes()) {
			if (personne.getEquipes().contains(equipe)) {
				row[0] = "" + i;
				row[1] = personne.getNom();
				row[2] = personne.getPrenom();
				row[3] = personne.getEmail();
				personneFromEquipeTableModel.addRow(row);
				i++;
			}
		}
	}

	public static JTable getPersonneToAddTable(Equipe equipe) {
		JTable personneToAdd = new JTable();
		DefaultTableModel personneTableModel = new DefaultTableModel(new String[] { "ID", "Nom de la personne",
				"Prénom de la personne", "Email de la personne", "Menu d'action" }, 0);
		updatePersonneToAddTableModel(equipe, personneTableModel);
		personneToAdd.setModel(personneTableModel);
		personneToAdd.getColumn("Menu d'action").setCellRenderer(new ButtonRenderer());
		personneToAdd.getColumn("Menu d'action").setCellEditor(new PersonneToAddButtonEditor(new JCheckBox(), equipe));
		personneToAdd.setPreferredSize(new Dimension(300, 300));
		return personneToAdd;
	}

	public static JTable getPersonneToDeleteTable(Equipe equipe) {
		JTable personneToRemove = new JTable();
		personneToDeleteTableModel = new DefaultTableModel(new String[] { "ID", "Nom de la personne",
				"Prénom de la personne", "Email de la personne", "Menu d'action" }, 0);
		updatePersonneToRemoveFromEquipeTableModel(equipe, personneToDeleteTableModel);
		personneToRemove.setModel(personneToDeleteTableModel);
		personneToRemove.getColumn("Menu d'action").setCellRenderer(new ButtonRenderer());
		personneToRemove.getColumn("Menu d'action")
				.setCellEditor(new PersonneToDeleteButtonEditor(new JCheckBox(), equipe));
		personneToRemove.setPreferredSize(new Dimension(300, 300));
		return personneToRemove;
	}

	public static JTable getPersonneFromEquipe(Equipe equipe) {
		JTable personneFromEquipe = new JTable();
		personneFromEquipeTableModel = new DefaultTableModel(new String[] { "ID", "Nom de la personne",
				"Prénom de la personne", "Email de la personne", "Menu d'action" }, 0);
		updatePersonneFromEquipeTableModel(equipe, personneFromEquipeTableModel);
		personneFromEquipe.setModel(personneFromEquipeTableModel);
		personneFromEquipe.setPreferredSize(new Dimension(300, 300));
		return personneFromEquipe;
	}
}
