package com.inscriptionsSportives.client.gui.form;

import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.client.gui.table.CompetitionTable;
import com.inscriptionsSportives.core.DateFormatter;
import com.inscriptionsSportives.core.GroupButtonUtils;
import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.util.Calendar;

public class CompetitionForm extends JPanel {

	private static final long serialVersionUID = -3152604217252474813L;
	private JPanel competitionContainerPanel;
    private JPanel competitionFormPanel;
    private JLabel nomCompetitionLabel;
    private JLabel dateDebutCompetitionLabel;
    private JLabel dateFinCompetitionLabel;
    private JLabel dateLimiteCompetitionLabel;
    private JTextField nomCompetitionTextField;
    private JDateChooser dateDebutCompetition;
    private JDateChooser dateFinCompetition;
    private JDateChooser dateLimiteInscription;
    private ButtonGroup choixButtonGroup;
    private JRadioButton enEquipeRadioButton;
    private JRadioButton enSoloRadioButton;
    private JButton addCompetitionButton;

    public CompetitionForm() {
        getNomCompetitionLabel();
        nomCompetitionTextField = new JTextField();
        dateDebutCompetitionLabel = new JLabel("Date de début");
        dateFinCompetitionLabel = new JLabel("Date de fin");
        getDateDebutCompetition();
        getDateFinCompetition();
        enEquipeRadioButton = new JRadioButton("En Équipe");
        enSoloRadioButton = new JRadioButton("En Solo");
        getChoixGroupeButton();
        getAddCompetitionButton();
        getCompetitionContainerPanel();
        add(competitionContainerPanel);
    }

    private JPanel getCompetitionContainerPanel(){
        competitionContainerPanel = new JPanel();
        competitionContainerPanel.setPreferredSize(new Dimension(360, 200));
        competitionContainerPanel.setBorder(BorderFactory.createLineBorder(Color.gray));
        competitionContainerPanel.add(getCompetitionFormPanel());
        return competitionContainerPanel;
    }

    private JButton getAddCompetitionButton(){
        addCompetitionButton = new JButton("Ajouter");
        Object[] row = new Object[6];
        addCompetitionButton.addActionListener(actionEvent -> {
        	LocalDate dateDebut = DateFormatter.dateToLocalDate(dateDebutCompetition.getDate());
            LocalDate dateFin = DateFormatter.dateToLocalDate(dateFinCompetition.getDate());
            LocalDate dateLimite = DateFormatter.dateToLocalDate(dateLimiteInscription.getDate());
            String nomCompetition = nomCompetitionTextField.getText();
        	row[0] = CompetitionTable.getCompetitionTableModel().getRowCount() + 1;
            row[1] = nomCompetitionTextField.getText();
            row[2] = DateFormatter.localDateToString(DateFormatter.dateToLocalDate(dateDebutCompetition.getDate()));
            row[3] = DateFormatter.localDateToString(DateFormatter.dateToLocalDate(dateFinCompetition.getDate()));
            row[4] = GroupButtonUtils.getSelectedButtonText(choixButtonGroup);
            boolean enEquipe = false;
            if (row[4] != null && row[4].equals("En Équipe")) {
                enEquipe = true;
            } else if (row[4] != null && row[4].equals("En Solo")) {
                enEquipe = false;
            } else {
                enEquipe = false;
                row[4] = "En Solo";
            }
            row[6] = DateFormatter.localDateToString(DateFormatter.dateToLocalDate(dateLimiteInscription.getDate()));
            Inscriptions.getInscriptions().createCompetition(nomCompetition,dateDebut, dateFin, enEquipe,dateLimite);
            CompetitionTable.getCompetitionTableModel().addRow(row);
        });
        return addCompetitionButton;
    }

    private JPanel getCompetitionFormPanel(){
        competitionFormPanel = new JPanel();
        competitionFormPanel.setLayout(new GridLayout(5, 3));
        competitionFormPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        competitionFormPanel.add(nomCompetitionLabel);
        competitionFormPanel.add(nomCompetitionTextField);
        competitionFormPanel.add(dateDebutCompetitionLabel);
        competitionFormPanel.add(dateDebutCompetition);
        competitionFormPanel.add(dateFinCompetitionLabel);
        competitionFormPanel.add(dateFinCompetition);
        competitionFormPanel.add(enEquipeRadioButton);
        competitionFormPanel.add(enSoloRadioButton);
        competitionFormPanel.add(addCompetitionButton);
        return competitionFormPanel;
    }

    private ButtonGroup getChoixGroupeButton(){
        choixButtonGroup = new ButtonGroup();
        choixButtonGroup.add(enEquipeRadioButton);
        choixButtonGroup.add(enSoloRadioButton);
        return choixButtonGroup;
    }

    private JDateChooser getDateFinCompetition(){
        dateFinCompetition = new JDateChooser();
        dateFinCompetition.setDate(Calendar.getInstance().getTime());
        return dateFinCompetition;
    }

    private JDateChooser getDateDebutCompetition(){
        dateDebutCompetition = new JDateChooser();
        dateDebutCompetition.setDate(Calendar.getInstance().getTime());
        return dateDebutCompetition;
    }

    private JLabel getNomCompetitionLabel(){
        nomCompetitionLabel = new JLabel("Nom de la compétition");
        nomCompetitionLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        return nomCompetitionLabel;
    }
}
