package com.inscriptionsSportives.client.gui.form;

import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.client.gui.table.PersonneTable;

import javax.swing.*;
import java.awt.*;

public class PersonneForm extends JPanel {

	private static final long serialVersionUID = -6543007249292335282L;
	private JPanel personneContainerPanel;
    private JPanel personneFormPanel;
    private JLabel nomPersonneLabel;
    private JTextField nomPersonneTextField;
    private JLabel prenomPersonneLabel;
    private JTextField prenomPersonneTextField;
    private JLabel emailPersonneLabel;
    private JTextField emailPersonneTextField;
    private JButton addPersonneButton;

    public PersonneForm(){

        personneContainerPanel = new JPanel();
        personneContainerPanel.setPreferredSize(new Dimension(360, 200));
        personneFormPanel = new JPanel();
        personneFormPanel.setLayout(new GridLayout(4, 0));
        nomPersonneLabel = new JLabel("Nom de la personne");
        nomPersonneLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        nomPersonneTextField = new JTextField();
        prenomPersonneLabel = new JLabel("Prénom");
        prenomPersonneLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        prenomPersonneTextField = new JTextField();
        emailPersonneLabel = new JLabel("E-mail");
        emailPersonneTextField = new JTextField();
        addPersonneButton = new JButton("Ajouter");
        Object[] row = new Object[3];
        addPersonneButton.addActionListener(actionEvent -> {
        	row[0] = ""+PersonneTable.getPersonneTableModel().getRowCount()+1;
            row[1] = nomPersonneTextField.getText();
            row[2] = prenomPersonneTextField.getText();
            row[3] = emailPersonneTextField.getText();
            Inscriptions.getInscriptions().createPersonne((String)row[1],(String)row[2],(String)row[3]);
            PersonneTable.getPersonneTableModel().addRow(row);
        });
        personneFormPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        personneContainerPanel.setBorder(BorderFactory.createLineBorder(Color.gray));

        personneFormPanel.add(nomPersonneLabel);
        personneFormPanel.add(nomPersonneTextField);
        personneFormPanel.add(prenomPersonneLabel);
        personneFormPanel.add(prenomPersonneTextField);
        personneFormPanel.add(emailPersonneLabel);
        personneFormPanel.add(emailPersonneTextField);
        personneFormPanel.add(addPersonneButton);

        personneContainerPanel.add(personneFormPanel);

        add(personneContainerPanel);


    }
}
