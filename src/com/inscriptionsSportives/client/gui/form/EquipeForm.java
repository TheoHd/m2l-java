package com.inscriptionsSportives.client.gui.form;

import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.client.gui.table.EquipeTable;

import javax.swing.*;
import java.awt.*;

public class EquipeForm extends JPanel{

	private static final long serialVersionUID = -7419482904286659259L;
	private JPanel equipeContainerPanel;
    private JPanel equipeFormPanel;
    private JLabel nomEquipeLabel;
    private JTextField nomEquipeTextField;
    private JButton addEquipeButton;

    public EquipeForm(){
        equipeContainerPanel = new JPanel();
        equipeContainerPanel.setPreferredSize(new Dimension(360, 200));
        equipeFormPanel = new JPanel();
        equipeFormPanel.setLayout(new GridLayout(2, 0));
        nomEquipeLabel = new JLabel("Nom de l'équipe");
        nomEquipeLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        nomEquipeTextField = new JTextField();
        addEquipeButton = new JButton("Ajouter");
        Object[] row = new Object[1];
        addEquipeButton.addActionListener(actionEvent->{
            row[0] = nomEquipeTextField.getText();
            Inscriptions.getInscriptions().createEquipe((String)row[0]);
            EquipeTable.getEquipeTableModel().addRow(row);
        });
        equipeFormPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        equipeContainerPanel.setBorder(BorderFactory.createLineBorder(Color.gray));

        equipeFormPanel.add(nomEquipeLabel);
        equipeFormPanel.add(nomEquipeTextField);
        equipeFormPanel.add(addEquipeButton);

        equipeContainerPanel.add(equipeFormPanel);

        add(equipeContainerPanel);
    }
}
