package com.inscriptionsSportives.client;

import com.inscriptionsSportives.client.gui.form.CompetitionForm;
import com.inscriptionsSportives.client.gui.form.EquipeForm;
import com.inscriptionsSportives.client.gui.form.PersonneForm;
import com.inscriptionsSportives.client.gui.tabbedPane.InscriptionsTabbedPane;
import com.inscriptionsSportives.client.gui.table.CompetitionTable;
import com.inscriptionsSportives.client.gui.table.EquipeTable;
import com.inscriptionsSportives.client.gui.table.PersonneTable;

import javax.swing.*;
import java.awt.*;

public class InterfaceGraphique extends JFrame {

	private static final long serialVersionUID = 3861809304270394756L;
	
	private static InterfaceGraphique interfaceGraphique;
	
	private JPanel topPanel;
	private JPanel bottomPanel;
	private JSplitPane splitPane;

	private InscriptionsTabbedPane inscriptionsTabbedPane;
	private CompetitionTable competitionTable;
	private EquipeTable equipeTable;
	private PersonneTable personneTable;

	private CompetitionForm competitionForm;
	private EquipeForm equipeForm;
	private PersonneForm personneForm;

	public InterfaceGraphique() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		createView();
		setTitle("Inscriptions Sportives");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setResizable(false);
	}

	private void createView() {
		splitPane = new JSplitPane();
		splitPane.setPreferredSize(new Dimension(1200, 720));
		getContentPane().setLayout(new GridLayout());
		getContentPane().add(splitPane);

		// Création et ajout de la partie supérieure de la fenêtre
		topPanel = new JPanel();
		competitionTable = new CompetitionTable();
		equipeTable = new EquipeTable();
		personneTable = new PersonneTable();
		inscriptionsTabbedPane = new InscriptionsTabbedPane(competitionTable, equipeTable, personneTable);
		topPanel.add(inscriptionsTabbedPane);

		// Création et ajout de la partie inférieure de la fenêtre
		bottomPanel = new JPanel();
		bottomPanel.setLayout(new GridLayout(0, 3));

		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane.setDividerLocation(320);
		splitPane.setTopComponent(topPanel);
		splitPane.setBottomComponent(bottomPanel);
		splitPane.setEnabled(false);

		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
		bottomPanel.setLayout(new FlowLayout());

		competitionForm = new CompetitionForm();
		equipeForm = new EquipeForm();
		personneForm = new PersonneForm();

		bottomPanel.add(competitionForm);
		bottomPanel.add(equipeForm);
		bottomPanel.add(personneForm);
	}
	
	public CompetitionTable getCompetitionTable() {
		return competitionTable;
	}

	public void setCompetitionTable(CompetitionTable competitionTable) {
		this.competitionTable = competitionTable;
	}

	public EquipeTable getEquipeTable() {
		return equipeTable;
	}

	public void setEquipeTable(EquipeTable equipeTable) {
		this.equipeTable = equipeTable;
	}

	public PersonneTable getPersonneTable() {
		return personneTable;
	}

	public void setPersonneTable(PersonneTable personneTable) {
		this.personneTable = personneTable;
	}

	public static InterfaceGraphique getInterfaceGraphique() {
		if(interfaceGraphique != null)
			return interfaceGraphique;
		else
			interfaceGraphique = new InterfaceGraphique();
			return interfaceGraphique;
		
	}
	
	public static void main(String args[]) {
		EventQueue.invokeLater(() -> getInterfaceGraphique().setVisible(true));
	}
}