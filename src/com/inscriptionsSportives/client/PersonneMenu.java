package com.inscriptionsSportives.client;

import com.inscriptionsSportives.app.*;
import com.inscriptionsSportives.core.DateFormatter;

import commandLineMenus.*;
import commandLineMenus.rendering.examples.util.InOut;

import java.time.LocalDate;
import java.util.ArrayList;

public class PersonneMenu {

	/*
	 * PARTIE PRINCIPALE
	 */

	static Menu getPersonneMenu() {
		Menu personneMenu = new Menu("- Menu Personne -", "Gérer les personnes", "3");
		personneMenu.add(getCreatePersonneOption());
		personneMenu.add(getModifyPersonneOption());
		personneMenu.add(getDeletePersonneOption());
		personneMenu.add(getReadMultiplePersonneOption());
		personneMenu.add(getNbCompetitionMoisOption());
		personneMenu.addBack("0");
		return personneMenu;
	}
	
	static Option getNbCompetitionMoisOption() {
		return new Option("Récupérer le nombre de compétitions des candidats en fonction d'un mois", "5", () -> {
			int jourCompetition = 30;
			int moisCompetition = InOut.getInt(" Mois (mm) : ");
			int anneeCompetition = InOut.getInt(" Année (yyyy) :");
			LocalDate dateDebut = LocalDate.of(anneeCompetition, moisCompetition, jourCompetition);
			for(Candidat cand : Inscriptions.getInscriptions().getCandidats()) {
				int nbCompetitionAuMois = cand.getNbCompetitionMois(dateDebut, cand.getId_ca());
				System.out.println("Nombre de compétition pour le candidat"+ cand.getNom() +" : "+nbCompetitionAuMois);
			}
		});
	}

	static Option getCreatePersonneOption() {
		return new Option("Créer une nouvelle personne", "1", () -> {
			String nom = InOut.getString("Nom de la personne: ");
			String prenom = InOut.getString("Prénom de la personne: ");
			String email = InOut.getString("E-mail de la personne: ");
			Inscriptions.getInscriptions().createPersonne(nom, prenom, email);
		});
	}

	static Option getModifyPersonneOption() {
		List<Personne> list = new List<>("Modifier une personne", "2",
				() -> new ArrayList<>(Inscriptions.getInscriptions().getPersonnes()),
				personne -> getModifyPersonneOptionAction(personne));

		list.addBack("0");
		return list;
	}

	static Option getDeletePersonneOption() {
		List<Personne> list = new List<>("Supprimer une personne", "3",
				() -> new ArrayList<>(Inscriptions.getInscriptions().getPersonnes()),
				(i, personne) -> personne.delete());
		list.addBack("0");
		return list;
	}

	static Option getReadMultiplePersonneOption() {
		return new Option("Voir toutes les personnes", "4",
				() -> System.out.println("-----\n" + Inscriptions.getInscriptions().getPersonnes()));
	}

	/*
	 * PARTIE MODIFICATION
	 */

	static Option getModifyPersonneOptionAction(Personne personne) {
		Menu modifyPersonneMenu = new Menu(personne.getNom());
		modifyPersonneMenu.add(getChangePersonneNameOption(personne));
		modifyPersonneMenu.add(getChangePersonneFirstnameOption(personne));
		modifyPersonneMenu.add(getChangePersonneEmailOption(personne));
		modifyPersonneMenu.addBack("0");
		return modifyPersonneMenu;
	}

	static Option getChangePersonneNameOption(Personne personne) {
		return new Option("Changer le nom de la personne", "1", () -> {
			personne.setNom(InOut.getString("Nouveau nom de la personne: "));
		});
	}

	static Option getChangePersonneFirstnameOption(Personne personne) {
		return new Option("Changer le prénom de la personne", "2", () -> {
			personne.setPrenom(InOut.getString("Nouveau prénom de la personne: "));
		});
	}

	static Option getChangePersonneEmailOption(Personne personne) {
		return new Option("Changer l'email de la personne", "3", () -> {
			personne.setEmail(InOut.getString("Nouvel e-mail de la personne: "));
		});
	}

}
