package com.inscriptionsSportives.client;

import com.inscriptionsSportives.app.*;

import commandLineMenus.*;
import commandLineMenus.rendering.examples.util.InOut;

import java.time.LocalDate;
import java.util.ArrayList;

public class CompetitionMenu {

	/*
	 * PARTIE PRINCIPALE
	 */
	static Menu getCompetitionMenu() {
		Menu competitionMenu = new Menu("- Menu Compétition -", "Gérer les compétitions", "1");
		competitionMenu.add(getCreateCompetitionOption());
		competitionMenu.add(getModifyCompetitionOption());
		competitionMenu.add(getDeleteCompetitionOption());
		competitionMenu.add(getReadMultipleCompetitionOption());
		competitionMenu.addBack("0");
		return competitionMenu;
	}

	static Option getCreateCompetitionOption() {
		return new Option("Créer une nouvelle compétition", "1", () -> {
			String nom = InOut.getString("Nom de la compétition: ");
			System.out.println("Début de la compétition :");
			int jourDebut = InOut.getInt("  Jour (jj) : ");
			int moisDebut = InOut.getInt("  Mois (mm) : ");
			int anneeDebut = InOut.getInt("  Année (yyyy) : ");
			System.out.println("Fin de la compétition :");
			int jourFin = InOut.getInt("  Jour (jj) : ");
			int moisFin = InOut.getInt("  Mois (mm) : ");
			int anneeFin = InOut.getInt("  Année (yyyy) : ");
			System.out.println("Date limite d'inscription:");
			int jourLimite = InOut.getInt("  Jour (jj) : ");
			int moisLimite = InOut.getInt("  Mois (mm) : ");
			int anneeLimite = InOut.getInt("  Année (yyyy) : ");
			int enEquipeSaisie = InOut.getInt("La compétition est-elle en équipe ? (0 = Non, 1 = Oui) ");
			boolean enEquipe = false;
			if (enEquipeSaisie == 1) {
				enEquipe = true;
			} else if (enEquipeSaisie != 0 && enEquipeSaisie != 1) {
				System.out.println("Erreur de saisie, veuillez entrer 1 ou 0 ! ");
			}
			LocalDate dateDebut = LocalDate.of(anneeDebut, moisDebut, jourDebut);
			LocalDate dateFin = LocalDate.of(anneeFin, moisFin, jourFin);
			LocalDate dateLimiteInscription = LocalDate.of(anneeLimite, moisLimite, jourLimite);
			Inscriptions.getInscriptions().createCompetition(nom, dateDebut, dateFin, enEquipe, dateLimiteInscription);
		});
	}

	static Option getModifyCompetitionOption() {
		List<Competition> list = new List<>("Modifier une compétition", "2",
				() -> new ArrayList<>(Inscriptions.getInscriptions().getCompetitions()),
				competition -> getModifyCompetitionOptionAction(competition));

		list.addBack("0");
		return list;
	}

	static Option getDeleteCompetitionOption() {
		List<Competition> list = new List<>("Supprimer une compétition", "3",
				() -> new ArrayList<>(Inscriptions.getInscriptions().getCompetitions()),
				(i, competition) -> competition.delete());
		list.addBack("0");
		return list;
	}

	static Option getReadMultipleCompetitionOption() {
		return new Option("Voir toutes les compétitions", "4",
				() -> System.out.println("-----\n" + Inscriptions.getInscriptions().getCompetitions()));
	}

	/*
	 * PARTIE MODIFICATION
	 */

	static Option getModifyCompetitionOptionAction(Competition competition) {
		Menu modifyCompetitionMenu = new Menu(competition.getNom() + " (" + competition.getCandidats().size() + ")");
		modifyCompetitionMenu.add(getChangeCompetitionNameOption(competition));
		modifyCompetitionMenu.add(getChangeDateCompetitionOption(competition));
		if (competition.isEnEquipe()) {
			modifyCompetitionMenu.add(getAddEquipeToCompetitionOption(competition));
			modifyCompetitionMenu.add(getRemoveEquipeFromCompetitionOption(competition));
		} else {
			modifyCompetitionMenu.add(getAddPersonneToCompetitionOption(competition));
			modifyCompetitionMenu.add(getRemovePersonneFromCompetitionOption(competition));
		}
		modifyCompetitionMenu.add(getReadParticipantCompetitionOption(competition));
		modifyCompetitionMenu.addBack("0");
		return modifyCompetitionMenu;
	}

	static Option getChangeCompetitionNameOption(Competition competition) {
		return new Option("Changer le nom de la compétition", "1", () -> {
			competition.setNom(InOut.getString("Nouveau nom de la compétition : "));
		});
	}

	static Option getChangeDateCompetitionOption(Competition competition) {
		return new Option("Changer la date de fin de la compétition", "2", () -> {
			System.out.println("Nouvelle date de fin de la compétition : ");
			int jour = InOut.getInt("  Jour (jj) : ");
			int mois = InOut.getInt("  Mois (mm) : ");
			int annee = InOut.getInt("  Année (yyyy) : ");
			LocalDate dateFin = LocalDate.of(annee, mois, jour);
			try {
				competition.setDateFin(dateFin);
			} catch (Exception e) {
				e.getMessage();
			}
		});
	}

	static Option getAddEquipeToCompetitionOption(Competition competition) {
		List<Equipe> list = new List<>("Ajouter une équipe à la compétition", "3", () -> {

			ArrayList<Equipe> possibleEquipeToAdd = new ArrayList<>();
			for (Equipe equipe : Inscriptions.getInscriptions().getEquipes()) {
				if (!competition.getCandidats().contains(equipe)) {
					possibleEquipeToAdd.add(equipe);
				}
			}
			return possibleEquipeToAdd;
		}, (i, equipe) -> competition.add(equipe));
		list.addBack("0");
		return list;
	}

	static Option getAddPersonneToCompetitionOption(Competition competition) {
		List<Personne> list = new List<>("Ajouter une personne à la compétition", "3", () -> {

			ArrayList<Personne> unePersonne = new ArrayList<>();
			for (Personne personne : Inscriptions.getInscriptions().getPersonnes())
				if (!competition.getCandidats().contains(personne))
					unePersonne.add(personne);

			return unePersonne;
		}, (i, personne) -> competition.add(personne));
		list.addBack("0");
		return list;
	}

	static Option getRemoveEquipeFromCompetitionOption(Competition competition) {
		List<Candidat> list = new List<>("Supprimer une équipe de la compétition", "4",
				() -> new ArrayList<>(competition.getCandidats()), (i, candidat) -> competition.remove(candidat));
		list.addBack("0");
		return list;
	}

	static Option getRemovePersonneFromCompetitionOption(Competition competition) {
		List<Candidat> list = new List<>("Supprimer une personne de la compétition", "4",
				() -> new ArrayList<>(competition.getCandidats()), (i, candidat) -> competition.remove(candidat));
		list.addBack("0");
		return list;
	}

	static Option getReadParticipantCompetitionOption(Competition competition) {
		return new Option("Afficher les participants de la compétition", "5",
				() -> System.out.println("-----\n" + competition.toString() + "-----\n" + competition.getCandidats()));
	}

}
