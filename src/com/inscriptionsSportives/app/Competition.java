package com.inscriptionsSportives.app;

import java.io.Serializable;
import java.util.Collections;
import java.time.LocalDate;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Représente une compétition, c'est-à-dire un ensemble de candidats
 * inscrits à un évènement, les inscriptions sont closes à la date dateCloture.
 */

@Entity
@Table(name = "competition")
public class Competition implements Comparable<Competition>, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_co")
    private int id_co;
    @Transient
    private static final long serialVersionUID = -2882150118573759729L;
    @Transient
    private Inscriptions inscriptions;
    @Column(name = "nom")
    private String nom;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "participer", joinColumns = {@JoinColumn(name = "id_co")}, inverseJoinColumns = {@JoinColumn(name = "id_ca")})
    private Set<Candidat> candidats;
    @Column(name = "date_debut")
    private LocalDate dateDebut;
    @Column(name = "date_fin")
    private LocalDate dateFin;
    @Column(name = "date_limite_insc")
    private LocalDate dateLimiteInscription;
    @Column(name = "en_equipe")
    private boolean enEquipe = false;
    
    public LocalDate getDateLimiteInscription() {
    	return this.dateLimiteInscription;
    }

    @SuppressWarnings("unused")
    private Competition() {
    }

    Competition(Inscriptions inscriptions, String nom, LocalDate dateDebut,LocalDate dateFin, boolean enEquipe, LocalDate dateLimiteInscription) {
        this.enEquipe = enEquipe;
        this.inscriptions = inscriptions;
        this.nom = nom;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        candidats = new TreeSet<>();
    }

    /**
     * Retourne le nom de la compétition.
     *
     * @return
     */

    public String getNom() {
        return nom;
    }

    /**
     * Modifie le nom de la compétition.
     */

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    /**
     * Permet de modifier la date de début de la compétition
     * @param localDate
     */
    public void setDateDebut(LocalDate dateDebut) {
    	this.dateDebut = dateDebut;
    }

    /**
     * Retourne vrai si les inscriptions sont encore ouvertes,
     * faux si les inscriptions sont closes.
     *
     * @return
     */

    public boolean inscriptionsOuvertes() {
        return (dateFin.isAfter(LocalDate.now()));
    }

    /**
     * Retourne la date de début des inscriptions.
     *
     * @return
     */

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    /**
     * Retourne la date de clôture des inscriptions.
     *
     * @return
     */

    public LocalDate getDateFin() {
        return dateFin;
    }

    /**
     * Retourne vrai si les inscriptions sont réservées aux équipes.
     *
     * @return
     */
    public boolean isEnEquipe() {
        return enEquipe;
    }
    
    /**
     * Permet de rendre une compétition en équipe ou non.
     * @param enEquipe
     */
    
    public void setEnEquipe(boolean enEquipe) {
    	this.enEquipe = enEquipe;
    }

    /**
     * Modifie la date de cloture des inscriptions. Il est possible de la reculer
     * mais pas de l'avancer.
     *
     * @param dateCloture
     * @throws Exception
     */

    public void setDateFin(LocalDate dateCloture) {
    	this.dateFin = dateCloture;
    }

    /**
     * Retourne l'ensemble des candidats inscrits.
     *
     * @return
     */

    public Set<Candidat> getCandidats() {
        return Collections.unmodifiableSet(candidats);
    }

    /**
     * Inscrit un candidat de type Personne à la compétition. Provoque une
     * exception si la compétition est réservée aux équipes ou que les
     * inscriptions sont closes.
     *
     * @param personne
     * @return
     */

    public boolean add(Personne personne) {
        if (dateFin.isBefore(LocalDate.now()))
            throw new RuntimeException();
        if (enEquipe)
            throw new RuntimeException();
        personne.add(this);
        return candidats.add(personne);
    }

    /**
     * Inscrit un candidat de type Equipe à la compétition. Provoque une
     * exception si la compétition est réservée aux personnes ou que
     * les inscriptions sont closes.
     *
     * @param equipe
     * @return
     */

    public boolean add(Equipe equipe) {
        if (dateFin.isBefore(LocalDate.now()))
            System.out.println("Impossible d'ajouter une équipe, la compétition est terminée !");
        if (!enEquipe)
        	System.out.println("Impossible d'ajouter une équipe, la compétition n'est pas en équipe !");
        equipe.add(this);
        boolean triggeredError = candidats.add(equipe);
        Inscriptions.getInscriptions().sauvegarder();
        return triggeredError;
    }

    /**
     * Désinscrit un candidat.
     *
     * @param candidat
     * @return
     */

    public boolean remove(Candidat candidat) {
    	if(LocalDate.now().isBefore(getDateLimiteInscription())) {
    		candidat.remove(this);
            return candidats.remove(candidat);
    	}else {
    		return false;
    	}
    }

    /**
     * Supprime la compétition de l'application.
     */

    public void delete() {
        for (Candidat candidat : candidats)
            remove(candidat);
        inscriptions.remove(this);
    }

    @Override
    public int compareTo(Competition o) {
        return getNom().compareTo(o.getNom());
    }

	@Override
    public String toString() {
        String etatCompetition = " ,la compétition n'est pas en équipe ";
        if(isEnEquipe())
            etatCompetition = " ,la compétition est en équipe ";
        return getNom() + " ferme le " + getDateFin() + etatCompetition +". La date limite d'inscription est le " +getDateLimiteInscription()+"\n";
    }

	public int getId_co() {
		return id_co;
	}
	
	

}