package com.inscriptionsSportives.app;

import java.io.*;
import java.util.*;
import java.time.LocalDate;

import com.inscriptionsSportives.persistance.Hibernate;

/**
 * Point d'entrée dans l'application, un seul objet de type Inscription permet
 * de gérer les compétitions, candidats (de type equipe ou personne) ainsi que
 * d'inscrire des candidats à des compétition.
 */
public class Inscriptions implements Serializable {

	private static final long serialVersionUID = -3095339436048473524L;
	private static final String FILE_NAME = "Inscriptions.srz";
	private static final boolean USE_HIBERNATE = true;
	private static Inscriptions inscriptions;
	private SortedSet<Competition> competitions = new TreeSet<>();
	private SortedSet<Candidat> candidats = new TreeSet<>();
	
	public static boolean useHibernate() {
		return USE_HIBERNATE;
	}

	private Inscriptions() {

	}

	private static Inscriptions readObject() {
		ObjectInputStream ois = null;
		try {
			FileInputStream fis = new FileInputStream(FILE_NAME);
			ois = new ObjectInputStream(fis);
			return (Inscriptions) (ois.readObject());
		} catch (IOException | ClassNotFoundException e) {
			return null;
		} finally {
			try {
				if (ois != null)
					ois.close();
			} catch (IOException e) {
				System.out.println("Impossible de fermer le fichier :   " + FILE_NAME);
				e.printStackTrace();
			}
		}

	}

	/**
	 * Créée une compétition. Ceci est le seul moyen, il n'y a pas de constructeur
	 * public dans {@link Competition}.
	 *
	 * @param nom
	 *            : Nom de la compétition
	 * @param dateFin
	 *            : Date de fin de la compétition
	 * @param enEquipe
	 *            : Booléen qui vérifie si la compétition est en équipe ou non
	 * @return Retourne la compétition créée.
	 */
	public Competition createCompetition(String nom, LocalDate dateDebut, LocalDate dateFin, boolean enEquipe,
			LocalDate dateLimiteInscription) {
		Competition competition = new Competition(this, nom, dateDebut, dateFin, enEquipe, dateLimiteInscription);
		competitions.add(competition);
		sauvegarder(competition);
		sauvegarder();
		return competition;
	}

	/**
	 * Créée une Candidat de type Personne. Ceci est le seul moyen, il n'y a pas de
	 * constructeur public dans {@link Personne}.
	 *
	 * @param nom
	 *            : Nom de la personne
	 * @param prenom
	 *            : Prénom de la personne
	 * @param mail
	 *            : Mail de la personne
	 * @return : Retourne la personne créée
	 */
	public Personne createPersonne(String nom, String prenom, String mail) {
		Personne personne = new Personne(this, nom, prenom, mail);
		candidats.add(personne);
		sauvegarder(personne);
		sauvegarder();
		return personne;
	}

	/**
	 * Créée une Candidat de type équipe. Ceci est le seul moyen, il n'y a pas de
	 * constructeur public dans {@link Equipe}.
	 *
	 * @param nom
	 *            : Nom de l'équipe
	 * @return : Retourne le nom de l'équipe
	 */
	public Equipe createEquipe(String nom) {
		Equipe equipe = new Equipe(this, nom);
		candidats.add(equipe);
		sauvegarder(equipe);
		sauvegarder();
		return equipe;
	}

	/**
	 * Retourne un object inscriptions vide. Ne modifie pas les compétitions et
	 * candidats déjà existants.
	 */
	// public static Inscriptions reinitialiser() {
	// inscriptions = new Inscriptions();
	// return getInscriptions();
	// }

	/**
	 * Efface toutes les modifications sur Inscriptions depuis la dernière
	 * sauvegarde. Ne modifie pas les compétitions et candidats déjà existants.
	 *
	 * @throws IOException
	 */
	// public static Inscriptions recharger() {
	// inscriptions = null;
	// return getInscriptions();
	// }

	/**
	 * Sauvegarde le gestionnaire pour qu'il soit ouvert automatiquement lors d'une
	 * exécution ultérieure du programme dans le cas où la sérialisation est
	 * activée. Dans le cas contraire, sauvegarde l'objet choisi en base de donnée.
	 */
	public void sauvegarder(Object o) {
		if (USE_HIBERNATE) {
			Hibernate.getHibernate().save(o);
		} else {
			serializationSave(null);
		}
	}

	private void serializationSave(ObjectOutputStream oos) {
		try {
			FileOutputStream fis = new FileOutputStream(FILE_NAME);
			oos = new ObjectOutputStream(fis);
			oos.writeObject(this);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (oos != null)
					oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Sauvegarde le gestionnaire pour qu'il soit ouvert automatiquement lors d'une
	 * exécution ultérieure du programme dans le cas où la sérialisation est
	 * activée. Dans le cas contraire, sauvegarde l'objet choisi en base de donnée.
	 */
	public void sauvegarder() {
		ObjectOutputStream oos = null;
		serializationSave(oos);
	}

	public static Inscriptions getInscriptions(boolean useHibernate) {
		if (useHibernate) {
			if (inscriptions == null) {
				inscriptions = readObject();
				if (inscriptions == null)
					inscriptions = new Inscriptions();
			}
			return inscriptions;
		} else {
			return getInscriptions();
		}
	}

	public static Inscriptions getInscriptions() {
		if (inscriptions == null) {
			inscriptions = readObject();
			if (inscriptions == null)
				inscriptions = new Inscriptions();
		}
		return inscriptions;
	}

	/**
	 * Supprimer un candidat
	 *
	 * @param candidat
	 *            : nom de l'équipe ou de la personne à supprimer
	 */
	public void remove(Candidat candidat) {
		candidats.remove(candidat);
		sauvegarder();
	}

	/**
	 * Supprime une compétition
	 *
	 * @param competition
	 *            : nom de la compétition à supprimer
	 */
	public void remove(Competition competition) {
		competitions.remove(competition);
		sauvegarder();
	}

	/**
	 * Retourne les compétitions.
	 *
	 * @return
	 */
	public SortedSet<Competition> getCompetitions() {
		return Collections.unmodifiableSortedSet(competitions);
	}

	/**
	 * Retourne tous les candidats (personnes et équipes confondues).
	 *
	 * @return
	 */
	public SortedSet<Candidat> getCandidats() {
		return Collections.unmodifiableSortedSet(candidats);
	}

	/**
	 * Retourne toutes les personnes.
	 *
	 * @return
	 */
	public SortedSet<Personne> getPersonnes() {
		SortedSet<Personne> personnes = new TreeSet<>();
		for (Candidat c : getCandidats())
			if (c instanceof Personne)
				personnes.add((Personne) c);
		return Collections.unmodifiableSortedSet(personnes);
	}

	/**
	 * Retourne toutes les équipes.
	 *
	 * @return
	 */
	public SortedSet<Equipe> getEquipes() {
		SortedSet<Equipe> equipes = new TreeSet<>();
		for (Candidat c : getCandidats())
			if (c instanceof Equipe)
				equipes.add((Equipe) c);
		return Collections.unmodifiableSortedSet(equipes);
	}

	public void setCandidats(List<Candidat> candidats) {
		this.candidats = (SortedSet<Candidat>) new TreeSet<Candidat>(candidats);
	}

	public void setCompetitions(List<Competition> competitions) {
		this.competitions = (SortedSet<Competition>) new TreeSet<Competition>(competitions);
	}

	@Override
	public String toString() {
		return "Candidats : " + getCandidats().toString() + "\nCompetitions  " + getCompetitions().toString();
	}

}