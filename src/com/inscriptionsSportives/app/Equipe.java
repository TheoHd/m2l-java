package com.inscriptionsSportives.app;

import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.*;

/**
 * Repr�sente une Equipe. C'est-à-dire un ensemble de personnes pouvant
 * s'inscrire à une comp�tition.
 */

@Entity
@Table(name = "equipe")
public class Equipe extends Candidat {
    @Transient
    private static final long serialVersionUID = 4147819927233466035L;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(
            name = "equipe_personne",
            joinColumns = {@JoinColumn(name = "equipe_id")},
            inverseJoinColumns = {@JoinColumn(name = "personne_id")}
    )
    @OrderBy("id_ca ASC")
    private SortedSet<Personne> membres = new TreeSet<>();
    
    private Equipe() {
    	
    }

    Equipe(Inscriptions inscriptions, String nom) {
        super(inscriptions, nom);
    }

    /**
     * Retourne l'ensemble des personnes formant l'�quipe.
     */

    public SortedSet<Personne> getMembres() {
        return Collections.unmodifiableSortedSet(membres);
    }

    /**
     * Ajoute une personne dans l'�quipe.
     *
     * @param membre
     * @return
     */

    public boolean add(Personne membre) {
        membre.add(this);
        return membres.add(membre);
    }

    /**
     * Supprime une personne de l'�quipe.
     *
     * @param membre
     * @return
     */

    public boolean remove(Personne membre) {
        membre.remove(this);
        return membres.remove(membre);
    }

    @Override
    public void delete() {
        super.delete();
    }

    @Override
    public String toString() {
    	if(getCompetitions().size() > 1)
    		return "L'équipe "+getNom()+" est inscrite aux compétitions "+getCompetitions() + "\n";
    	else
    		return "L'équipe "+getNom()+" est inscrite à la compétition "+getCompetitions() + "\n";
    	
    }

}
