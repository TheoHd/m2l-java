package com.inscriptionsSportives.app;

import com.inscriptionsSportives.core.Functions;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class MailExemple {

	private static final String host = "localhost";
	private static final String mailHost = "mail.smtp.host";
	private static final String mailSmtpPort = "mail.smtp.port";
	private static final String mailSmtpSocketFactoryPort = "mail.smtp.socketFactory.port";
	private static final String mailSmtpSocketFactoryClass = "mail.smtp.socketFactory.class";
	private static final String mailSmtpAuth = "mail.smtp.auth";
	private static final String port = "465";

	private String recipient = "recipient@host.com";
	private String sender = "sender@gmail.com";
	private String subject = "Subject";
	private String content = "Mail content";

	private Properties properties;
	private Session session;

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void sendMail(String sender, String recipient, String subject, String text) {
		if (Functions.validateEmail(sender) && Functions.validateEmail(recipient)) {
			// Récupération des propriétés système
			setProperties(System.getProperties());
			// Mise en place du serveur mail
			properties.setProperty(mailHost, host);
			properties.put(mailHost, "smtp.gmail.com");
			properties.put(mailSmtpSocketFactoryPort, port);
			properties.put(mailSmtpSocketFactoryClass, "javax.net.ssl.SSLSocketFactory");
			properties.put(mailSmtpAuth, "true");
			properties.put(mailSmtpPort, port);
			// Récupération de l'objet Session par défaut
			setSession(Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("email", "password");
				}
			}));

			try {
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(sender));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
				message.setSubject(subject);
				message.setText(text);
				Transport.send(message);
				System.out.println("Done");
			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public static void main(String args[]) {
		MailExemple mail = new MailExemple();
		mail.sendMail(mail.getSender(), mail.getRecipient(), mail.getSubject(), mail.getContent());
	}

	private void setProperties(Properties properties) {
		this.properties = properties;
	}

	private void setSession(Session session) {
		this.session = session;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getSubject() {
		return subject;
	}
}