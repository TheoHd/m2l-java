package com.inscriptionsSportives.app;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import javax.persistence.*;

/**
 * Repr�sente une personne physique pouvant s'inscrire � une comp�tition.
 */

@Entity
@Table(name = "personne")
public class Personne extends Candidat {
	@Transient
	private static final long serialVersionUID = 4434646724271327254L;
	@Column(name = "prenom")
	private String prenom;
	@Column(name = "mail")
	private String mail;
	@ManyToMany(mappedBy = "membres")
	@OrderBy("id_ca ASC")
	private Set<Equipe> equipes;

	Personne(Inscriptions inscriptions, String nom, String prenom, String mail) {
		super(inscriptions, nom);
		this.prenom = prenom;
		this.mail = mail;
		equipes = new TreeSet<>();
	}

	public Personne() {
		super();
	}

	/**
	 * Retourne le pr�nom de la personne.
	 *
	 * @return
	 */

	public String getPrenom() {
		return prenom;
	}

	/**
	 * Modifie le prénom de la personne.
	 *
	 * @param prenom
	 */

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Retourne l'adresse électronique de la personne.
	 *
	 * @return
	 */

	public String getEmail() {
		return mail;
	}

	/**
	 * Modifie l'adresse électronique de la personne.
	 *
	 * @param mail
	 */

	public void setEmail(String mail) {
		this.mail = mail;
	}

	/**
	 * Retoure les �quipes dont cette personne fait partie.
	 *
	 * @return
	 */

	public Set<Equipe> getEquipes() {
		return Collections.unmodifiableSet(equipes);
	}

	boolean add(Equipe equipe) {
		return equipes.add(equipe);
	}

	boolean remove(Equipe equipe) {
		return equipes.remove(equipe);
	}

	@Override
	public void delete() {
		super.delete();
		for (Equipe e : equipes)
			e.remove(this);
	}

	@Override
	public String toString() {
		int id = getId_ca();
		String prenom = getPrenom();
		String nom = getNom();
		String equipe = equipes.toString();
		String email = getEmail();
		String resultat;
		if (equipe.equals("") || equipe == null) {
			if (email.equals("") || email == null)
				resultat = id + ". " + prenom + " " + nom
						+ " n'est membre d'aucune équipe. Il n'a pas souhaité communiquer son adresse-email.";
			else
				resultat = id + ". " + prenom + " " + nom + " n'est membre d'aucune équipe. Son adresse e-mail est: "
						+ email + ".";
		} else {
			if (email.equals("") || email == null)
				resultat = id + ". " + prenom + " " + nom + " est membre de l'équipe " + "\"" + equipe
						+ "\". Il n'a pas souhaité communiquer son adresse-email.";
			else
				resultat = id + ". " + prenom + " " + nom + " est membre de l'équipe " + "\"" + equipe
						+ "\". Son adresse e-mail est: " + email;
		}
		return resultat;
	}
}
