package com.inscriptionsSportives.app;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;
import java.util.TreeSet;
import java.util.Collections;

import javax.persistence.*;

/**
 * Candidat à un évènement sportif, soit une personne physique, soit une équipe.
 */
@Entity
@Table(name = "candidat")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Candidat implements Comparable<Candidat>, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_ca", nullable = false)
    private int id_ca;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "participer", joinColumns = {@JoinColumn(name = "id_ca")}, inverseJoinColumns = {@JoinColumn(name = "id_co")})
    private Set<Competition> competitions;
    @Column(name = "nom")
    private String nom;
    @Transient
    private Inscriptions inscriptions;
    @Transient
    private static final long serialVersionUID = -6035399822298694746L;

    protected Candidat() {
    }

    Candidat(Inscriptions inscriptions, String nom) {
        this.inscriptions = inscriptions;
        this.nom = nom;
        competitions = new TreeSet<>();
    }

    public String getNom() {
        return nom;
    }

    /**
     * Modifie le nom du candidat.
     *
     * @param nom_ca nom du candidat
     */
    public void setNom(String nom_ca) {
        this.nom = nom_ca;
    }

    /**
     * @return Retourne toutes les comp�titions auxquelles ce candidat est inscrit.
     */
    public Set<Competition> getCompetitions() {
        return Collections.unmodifiableSet(competitions);
    }

    void add(Competition competition) {
        competitions.add(competition);
    }

    void remove(Competition competition) {
        competitions.remove(competition);
    }

    /**
     * Supprime un candidat de l'application.
     */
    public void delete() {
        for (Competition c : competitions)
            c.remove(this);
        inscriptions.remove(this);
    }
    
    /**
     * Retourne le nombre de competition du mois pr�cis�
     * @param date
     * @param idCandidat
     * @return
     */
    public int getNbCompetitionMois(LocalDate date, int idCandidat) {
    	int count = 0;
    	for(Competition comp : this.getCompetitions()) {
    		if(comp.getDateDebut().isBefore(date)) {
    			count++;		
    		}    			
    	}
    	return count;
    }

    public int getId_ca() {
        return id_ca;
    }

    @Override
    public int compareTo(Candidat o) {
        return getNom().compareTo(o.getNom());
    }

    @Override
    public String toString() {
        return "\n" + getNom() + " -> inscrit � " + getCompetitions();
    }
}
