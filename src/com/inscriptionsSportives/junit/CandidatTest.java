package com.inscriptionsSportives.junit;

import static org.junit.Assert.*;
import java.time.LocalDate;
import org.junit.Test;
import com.inscriptionsSportives.app.Competition;
import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.app.Personne;

public class CandidatTest {

    @Test
    public void testGetNom() {
        Inscriptions inscriptions = Inscriptions.getInscriptions();
        Personne Personne = inscriptions.createPersonne("a", "b", "c");
        String inscri = Personne.getPrenom();
        assertEquals("b", inscri);
    }

    @Test
    public void testSetNom() {
        Inscriptions inscriptions = Inscriptions.getInscriptions();
        Personne Personne = inscriptions.createPersonne("a", "b", "c");
        Personne.setNom("a");
        String inscri = Personne.getNom();
        assertEquals("a", inscri);
    }

    @Test
    public void testGetCompetitions() {
        Inscriptions inscriptions = Inscriptions.getInscriptions();
        Personne Personne = inscriptions.createPersonne("a", "b", "c");
        Competition Compet = inscriptions.createCompetition("Compet", LocalDate.now(),LocalDate.now().plusDays(20), false,LocalDate.now().plusDays(40));
        Competition Compet1 = inscriptions.createCompetition("Compet", LocalDate.now(),LocalDate.now().plusDays(20), false,LocalDate.now().plusDays(40));
        Compet.add(Personne);
        Compet1.add(Personne);
        assertTrue(Personne.getCompetitions().contains(Compet));
        assertTrue(Personne.getCompetitions().contains(Compet1));
    }

    @Test
    public void testDelete() {
        Inscriptions inscriptions = Inscriptions.getInscriptions();
        Personne p = inscriptions.createPersonne("Yeah", "Ya", "Yo");
        p.delete();
        assertTrue(!inscriptions.getCandidats().contains(p));
    }


    @Test
    public void testCompareTo() {
        Inscriptions inscriptions = Inscriptions.getInscriptions();
        Personne p1 = inscriptions.createPersonne("Ha", "Ho", "He");
        Personne p2 = inscriptions.createPersonne("Ha", "Ho", "He");
        assertEquals(0, p1.compareTo(p2));
    }

    @Test
    public void testToString() {
        Inscriptions inscriptions = Inscriptions.getInscriptions();
        Personne p1 = inscriptions.createPersonne("Test", "Aby", "Bonjour");
        Personne p2 = inscriptions.createPersonne("Test", "Th�o", "Bonsoir");
        assertNotNull(p1.toString());
        assertNotNull(p2.toString());
    }

}
