package com.inscriptionsSportives.junit;

import static org.junit.Assert.*;
import org.junit.Test;
import com.inscriptionsSportives.app.*;

public class EquipeTest {
	
	@Test
	public void testDelete()  {
		Inscriptions inscriptions = Inscriptions.getInscriptions();
		Personne personne = inscriptions.createPersonne("a", "b", "c");
		Equipe equipe = inscriptions.createEquipe("d");
		equipe.add(personne);
		assertTrue(inscriptions.getPersonnes().contains(personne));
		personne.delete();
		assertTrue(!equipe.getMembres().contains(personne));
		assertTrue(!inscriptions.getPersonnes().contains(personne));
		assertTrue(!inscriptions.getCandidats().contains(personne));
	}

	@Test
	public void testGetMembres()  {
		Inscriptions inscriptions = Inscriptions.getInscriptions();
		Personne personne = inscriptions.createPersonne("a", "b", "c");
		Personne personne1 = inscriptions.createPersonne("c", "d", "e");
		Equipe equipe = inscriptions.createEquipe("d");
		equipe.add(personne);
		equipe.add(personne1);
		int size = equipe.getMembres().size();
		assertTrue(equipe.getMembres().contains(personne));
		assertTrue(equipe.getMembres().contains(personne1));
		assertEquals(size,equipe.getMembres().size());
	}

	@Test
	public void testAddPersonne()  {
		Inscriptions inscriptions = Inscriptions.getInscriptions();
		Personne personne = inscriptions.createPersonne("a", "b", "c");
		Personne personne1 = inscriptions.createPersonne("d", "e", "f");
		Equipe equipe = inscriptions.createEquipe("g");
		equipe.add(personne);
		equipe.add(personne1);
		assertTrue(equipe.getMembres().contains(personne));
		assertTrue(equipe.getMembres().contains(personne1));
	}

	@Test
	public void testRemovePersonne()  {
		Inscriptions inscriptions = Inscriptions.getInscriptions();
		Personne personne = inscriptions.createPersonne("a", "b", "c");
		Equipe equipe = inscriptions.createEquipe("d");
		equipe.add(personne);
		assertTrue(equipe.getMembres().contains(personne));
		personne.delete();
		assertTrue(!equipe.getMembres().contains(personne));
	}
	
	@Test
	public void testToString()  {
		Inscriptions inscriptions = Inscriptions.getInscriptions();
		Personne personne = inscriptions.createPersonne("a", "b", "c");
		Equipe equipe = inscriptions.createEquipe("d");
		equipe.add(personne);
		assertNotNull(personne.toString());
	}
}
