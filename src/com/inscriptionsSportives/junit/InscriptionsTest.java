package com.inscriptionsSportives.junit;

import static org.junit.Assert.*;
import org.junit.Test;
import java.time.LocalDate;
import com.inscriptionsSportives.app.Competition;
import com.inscriptionsSportives.app.Equipe;
import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.app.Personne;

public class InscriptionsTest {


    @Test
    public void testGetPersonnes() {
        Inscriptions inscription = Inscriptions.getInscriptions();
        Personne Personne1 = inscription.createPersonne("d", "e", "f");
        assertTrue(inscription.getPersonnes().contains(Personne1));
    }

    @Test
    public void testGetCompetitions() {
        Inscriptions inscription = Inscriptions.getInscriptions();
        Competition compet = inscription.createCompetition("a", LocalDate.now(),LocalDate.now().plusDays(10), true,LocalDate.now().plusDays(20));
        Competition compet1 = inscription.createCompetition("b", LocalDate.now(),LocalDate.now().plusDays(10), true,LocalDate.now().plusDays(20));
        assertTrue(inscription.getCompetitions().contains(compet));
        assertTrue(inscription.getCompetitions().contains(compet1));
    }

    @Test
    public void testGetCandidats() {
        Inscriptions inscri = Inscriptions.getInscriptions();
        Personne Personne = inscri.createPersonne("a", "b", "c");
        Personne Personne1 = inscri.createPersonne("d", "e", "f");
        Personne Personne2 = inscri.createPersonne("g", "h", "i");
        Equipe Equipe = inscri.createEquipe("j");
        Equipe.add(Personne1);
        Equipe.add(Personne2);
        assertTrue(inscri.getCandidats().contains(Personne));
        assertTrue(inscri.getCandidats().contains(Equipe));
    }

    @Test
    public void testGetEquipes() {
        Inscriptions inscri = Inscriptions.getInscriptions();
        Equipe Equipe = inscri.createEquipe("a");
        Equipe Equipe1 = inscri.createEquipe("b");
        assertTrue(inscri.getCandidats().contains(Equipe));
        assertTrue(inscri.getCandidats().contains(Equipe1));
    }


    @Test
    public void testCreateCompetition() {
        Inscriptions inscri = Inscriptions.getInscriptions();
        Competition competition = inscri.createCompetition("a", LocalDate.now(),LocalDate.now().plusDays(10), true,LocalDate.now().plusDays(20));
        assertTrue(inscri.getCompetitions().contains(competition));
    }

    @Test
    public void testCreatePersonne() {
        Inscriptions inscri = Inscriptions.getInscriptions();
        Personne Personne = inscri.createPersonne("a", "b", "c");
        assertTrue(inscri.getCandidats().contains(Personne));
    }


    @Test
    public void testCreateEquipe() {
        Inscriptions inscri = Inscriptions.getInscriptions();
        Equipe Equipe = inscri.createEquipe("a");
        assertTrue(inscri.getCandidats().contains(Equipe));

    }


    @Test
    public void testGetInscriptions() {
        Inscriptions inscri = Inscriptions.getInscriptions();
        assertNotNull(inscri);
    }

    @Test
    public void testReinitialiser() {
        Inscriptions inscriptions = Inscriptions.getInscriptions();
        assertNotNull("Une inscription a �t� r�initialis�e", inscriptions);
    }

    @Test
    public void testRecharger() {
        Inscriptions inscriptions = null;
        inscriptions = Inscriptions.getInscriptions();
        assertNotNull("L'application a bien été réinitialisée", inscriptions);
    }


    @Test
    public void testToString() {
        Inscriptions inscriptions = Inscriptions.getInscriptions();
        assertNotNull(inscriptions.toString());
    }

}
