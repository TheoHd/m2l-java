package com.inscriptionsSportives.junit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.time.LocalDate;
import org.junit.Test;
import com.inscriptionsSportives.app.Competition;
import com.inscriptionsSportives.app.Equipe;
import com.inscriptionsSportives.app.Inscriptions;
import com.inscriptionsSportives.app.Personne;

public class CompetitionTest {

    @Test
    public void testGetNom() {
        com.inscriptionsSportives.app.Inscriptions inscriptions = com.inscriptionsSportives.app.Inscriptions.getInscriptions();
        Competition compet = inscriptions.createCompetition("Tournoi de fl�chettes", LocalDate.now(),null, true,LocalDate.now().plusDays(20));
        assertEquals("Tournoi de fl�chettes", compet.getNom());
    }

    @Test
    public void testSetNom() {
        com.inscriptionsSportives.app.Inscriptions inscriptions = com.inscriptionsSportives.app.Inscriptions.getInscriptions();
        Competition compet = inscriptions.createCompetition("gertrude", LocalDate.now(),null, true,LocalDate.now().plusDays(20));
        compet.setNom("gertrude");
        assertEquals("gertrude", compet.getNom());
    }

    @Test
    public void testGetCandidats() {
        Inscriptions inscription = Inscriptions.getInscriptions();
        Competition compet = inscription.createCompetition("testCompet", LocalDate.now(),LocalDate.now(), false,LocalDate.now().plusDays(20));
        Personne aby = inscription.createPersonne("ha", "oh", "eh");
        Personne theo = inscription.createPersonne("ha", "oh", "eh");
        compet.add(aby);
        compet.add(theo);
        assertTrue(compet.getCandidats().contains(aby));
        assertTrue(compet.getCandidats().contains(theo));
    }

    @Test
    public void testAddEquipe() {
        Inscriptions inscriptions = Inscriptions.getInscriptions();
        Competition c = inscriptions.createCompetition("test", LocalDate.now(),LocalDate.now(), true,LocalDate.now().plusDays(20));
        Equipe e1 = inscriptions.createEquipe("testTeam");
        Equipe e2 = inscriptions.createEquipe("testTeam");   
        c.add(e1);
        c.add(e2);
        assertTrue(c.getCandidats().contains(e1));
        assertTrue(c.getCandidats().contains(e2));
    }

    @Test
    public void testRemove() {
        Inscriptions inscri = Inscriptions.getInscriptions();
        Competition compet = inscri.createCompetition("test", LocalDate.now(),LocalDate.now(), false,LocalDate.now().plusDays(20));
        Personne Personne = inscri.createPersonne("nom", "prenom", "mail");
        Personne Personne2 = inscri.createPersonne("nom", "prenom", "mail");
        compet.add(Personne);
        compet.add(Personne2);
        int sizeBefore = compet.getCandidats().size();
        compet.remove(Personne);
        int sizeAfter = compet.getCandidats().size();
        assertEquals(sizeBefore - 1, sizeAfter);
    }

    @Test
    public void testDelete() {
        Inscriptions inscriptions = Inscriptions.getInscriptions();
        Competition compet = inscriptions.createCompetition("Compet",LocalDate.now(), LocalDate.now().plusDays(20), false,LocalDate.now().plusDays(20));
        int size = inscriptions.getCompetitions().size();
        compet.delete();
        assertEquals(size - 1, inscriptions.getCompetitions().size());
    }

    @Test
    public void testCompareTo() {
        Inscriptions inscriptions = Inscriptions.getInscriptions();
        Competition compet = inscriptions.createCompetition("Compet",LocalDate.now(), LocalDate.now().plusDays(20), false,LocalDate.now().plusDays(20));
        Competition compet2 = inscriptions.createCompetition("Compet", LocalDate.now(),LocalDate.now().plusDays(20), false,LocalDate.now().plusDays(20));
        assertEquals(0, compet.compareTo(compet2));
    }

    @Test
    public void testToString() {
        Inscriptions inscriptions = Inscriptions.getInscriptions();
        Competition compet = inscriptions.createCompetition("compet", LocalDate.now(),LocalDate.now().plusDays(20), false,LocalDate.now().plusDays(20));
        assertNotNull(compet.toString());
    }


}
