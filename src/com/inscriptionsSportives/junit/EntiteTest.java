package com.inscriptionsSportives.junit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.junit.Assert.*;

import org.junit.Test;

public class EntiteTest {

    @Test
    public void testGetConnection() throws SQLException {
        String jdbcUrl = "jdbc:mysql://localhost:3306/inscriptions_sportives_theo_abigaelle?useSSL=false";
        String user = "root";
        String pass = "";
        Connection myConn = DriverManager.getConnection(jdbcUrl, user, pass);
        assertNotNull(myConn);
    }
}

